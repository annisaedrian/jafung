<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {
  Route::get('/home', 'HomeController@index')->name('home');

  Route::get('export', 'ExcelController@Export');

  Route::get('penelitian/excel/{type}', 'ExcelController@penelitian');

  Route::get('pengabdian/excel/{type}', 'ExcelController@pengabdian');

  Route::get('dosen/excel/{type}', 'ExcelController@dosen');

  Route::get('penunjang/excel/{type}', 'ExcelController@penunjang');

  Route::get('dupak/excel/{type}', 'ExcelController@dupak');

  Route::put('detail_penunjang/{id}', 'PenunjangController@update_detail');

  Route::delete('detail_penunjang/{id}', 'PenunjangController@destroy_detail');

  Route::put('detail_unsurpendidikan/{id}', 'UnsurPendidikanController@update_detail');

  Route::delete('detail_unsurpendidikan/{id}', 'UnsurPendidikanController@destroy_detail');

  Route::put('detail_pelaksanapendidikan/{id}', 'PelaksanaPendidikanController@update_detail');

  Route::delete('detail_pelaksanapendidikan/{id}', 'PelaksanaPendidikanController@destroy_detail');

  Route::put('detail_penelitian/{id}', 'PenelitianController@update_detail');

  Route::delete('detail_penelitian/{id}', 'PenelitianController@destroy_detail');

  Route::put('detail_pengabdian/{id}', 'PengabdianController@update_detail');

  Route::delete('detail_pengabdian/{id}', 'PengabdianController@destroy_detail');

  Route::post('penelitian/find', 'PenelitianController@find');

  Route::post('penunjang/find', 'PenunjangController@find');

  Route::post('pengabdian/find', 'PengabdianController@find');

  Route::post('unsurpendidikan/find', 'UnsurPendidikanController@find');

  Route::post('pelaksanapendidikan/find', 'PelaksanaPendidikanController@find');

  Route::resource('dosen', 'DosenController');

  Route::resource('penelitian_detail', 'DetailPenelitianController');

  Route::resource('unsurpendidikan', 'UnsurPendidikanController');

  Route::resource('pendidikan', 'PendidikanController');

  Route::resource('pelaksanapendidikan', 'PelaksanaPendidikanController');

  Route::resource('penelitian', 'PenelitianController');

  Route::resource('pengabdian', 'PengabdianController');

  Route::resource('penunjang', 'PenunjangController');

  Route::resource('dupak', 'DupakController');

  Route::get('penelitian/downloadPDF/{id}','PenelitianController@downloadPDF');

  Route::get('pengabdian/downloadPDF/{id}','PengabdianController@downloadPDF');

  Route::get('penunjang/downloadPDF/{id}','PenunjangController@downloadPDF');

  Route::get('pendidikan/downloadPDF/{id}','PendidikanController@downloadPDF');

  Route::get('dupak/downloadPDF/{id}','DupakController@downloadPDF');
});

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>DAFTAR KARYA ILMIAH - {{ $data->name }}</title>
  </head>
  <body>
    <div class="p-t-50 p-b-50 p-l-50 p-r-50">
      <center><font size="3" face="Times New Roman"><b><u>DAFTRA KARYA ILMIAH</u></b></font></center>
      <br>
      <div class="column">
          <div class="columns">
            <div class="column">
              <font size="2" face="Times New Roman">PEGAWAI NEGERI SIPIL/DOSEN YANG DINILAI :</font>
            </div>
          </div>
       <br>
       <table class="table is-bordered is-striped is-narrow is-fullwidth">
         <tr>
           <td>
             <font size="2" face="Times New Roman">1.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N a m a </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->name}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">2.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N I P / NIDN </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->nip}} , {{$data->nidn}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">2.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Pangkat / Gol. Ruang </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->pangkat}} , {{$data->gol_ruang}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">4.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Jabatan Fungsional </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->jab_fungsional}} , {{$data->tmt}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">5.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Unit Kerja </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->unit_kerja}} </font>
           </td>
         </tr>
       </table>

     </div>
    <table class="table is-bordered is-striped is-narrow is-fullwidth">
      <link rel="stylesheet" href="{{ public_path('css/app.css') }}">
      <tr>
        <td rowspan="2">
          <font size="2" face="Times New Roman">No</font>
        </td>
        <td rowspan="2">
          <font size="2" face="Times New Roman">Judul Karya Ilmiah</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Sub Unsur</font>
        </td>
        <td colspan="2">
          <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
        </td>
        <td rowspan="2">
          <font size="2" face="Times New Roman">Ket. / Bukti Fisik</font>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman">*)Nilai Angka Kredit</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tim Penilai Perguruan Tinggi</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tim Penilai Pusat  </font>
        </td>
      </tr>

      <tr>
        <td colspan="6">
          <font size="2" face="Times New Roman">Menghasilkan karya ilmiah dipublikasikan :</font>
        </td>
      </tr>

      @for ($i = 0; $i < count($data->detail_penelitian); $i++)
        <tr>
        <td><font size="2" face="Times New Roman">{{$i+1}}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penelitian[$i]->judul_karya }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penelitian[$i]->nilai_kredit }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penelitian[$i]->penilai_perguruan }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penelitian[$i]->penilai_pusat }}</font></td>
        <td><font size="2" face="Times New Roman">B{{$i+1}}</font></td>
        </tr>
      @endfor
      <tr>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>Jumlah</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>{{ $kredit }}</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>{{ $kredit_perguruan}}</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"><b>{{ $kredit_pusat}}</b></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
      </tr>
    </table>
    <div class="column">
      <font size="2" face="Times New Roman">Demikianlah pernyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</font>
    </div>
    <div class="column">
      <div style="text-align: right;">
        <font size="2" face="Times New Roman"> Jakarta, {{ $date->toFormattedDateString() }}</font><br>
        <font size="2" face="Times New Roman">Ketua STMIK Swadharma</font><br>
        <br>
        <br>
        <br>
        <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
      </div>
    </div>
    </div>
  </body>
</html>

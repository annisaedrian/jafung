<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Kegiatan Penunjang Tugas Pokok Dosen - {{ $data->name }}</title>
  </head>
  <body>
    <div class="p-t-50 p-b-50 p-l-50 p-r-50">
      <center><font size="3" face="Times New Roman"><b>SURAT PERNYATAAN</b></font></center>
      <center><font size="3" face="Times New Roman"><b>MELAKUKAN KEGIATAN PENUNJANG TUGAS POKOK DOSEN</b></font></center><br>
      <div class="column">
        <font size="2" face="Times New Roman">Yang bertanda tangan di bawah ini :</font>
      </div>
      <table class="table is-bordered is-striped is-narrow is-fullwidth">
        <tr>
          <td>
            <font size="2" face="Times New Roman">1.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">N a m a </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">2.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">N I P / NIDN </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->nip}} , {{$a[0]->nidn}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">3.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Pangkat / Gol. Ruang </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->pangkat}} , {{$a[0]->gol_ruang}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">4.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Jabatan Fungsional </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->jab_fungsional}} , {{$a[0]->tmt}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">5.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Unit Kerja </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->unit_kerja}} </font>
          </td>
        </tr>
      </table>
      <div class="column">
        <font size="2" face="Times New Roman">Menyatakan bahwa :</font>
      </div>
       <table class="table is-bordered is-striped is-narrow is-fullwidth">
         <tr>
           <td>
             <font size="2" face="Times New Roman">6.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N a m a </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->name}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">7.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N I P / NIDN </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->nip}} , {{$data->nidn}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">8.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Pangkat / Gol. Ruang </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->pangkat}} , {{$data->gol_ruang}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">9.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Jabatan Fungsional </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->jab_fungsional}} , {{$data->tmt}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">10.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Unit Kerja </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$data->unit_kerja}} </font>
           </td>
         </tr>
       </table>
      <div class="column">
        <font size="2" face="Times New Roman">Telah melakukan kegiatan penunjang tugas pokok dosen :</font>
      </div>
    <table class="table is-bordered is-striped is-narrow is-fullwidth">
      <link rel="stylesheet" href="{{ public_path('css/app.css') }}">
      <tr>
        <td>
          <font size="2" face="Times New Roman">No  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Kegiatan Penunjang Tri Dharma PT</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Kedudukan / Tingkat</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tempat / Instansi  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tanggal  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah Angka Kredit  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Ket. / Bukti Fisik  </font>
        </td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">1</font></td>
        <td><font size="2" face="Times New Roman">Menjadi anggota dalam suatu panitia/badan perguruan tinggi </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">2</font></td>
        <td><font size="2" face="Times New Roman">Menjadi anggota panitia/badan pada lembaga pemerintahan</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">3</font></td>
        <td><font size="2" face="Times New Roman">Menjadi anggota organisasi profesi</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">4</font></td>
        <td><font size="2" face="Times New Roman">Mewakili Perguruan Tinggi/Lembaga Pemerintah duduk dalam Panitia Antar Lembaga</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">5</font></td>
        <td><font size="2" face="Times New Roman">Menjadi anggota delegasi Nasional ke pertemuan Internasional</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman">6</font></td>
        <td><font size="2" face="Times New Roman">Berperan serta aktif dalam pertemuan ilmiah :</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      @for ($i = 0; $i < count($data->detail_penunjang); $i++)
        <tr>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $data->detail_penunjang[$i]->kegiatan_penunjang }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penunjang[$i]->kedudukan }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penunjang[$i]->tempat }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penunjang[$i]->tgl }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $data->detail_penunjang[$i]->jml_kredit }}</font></td>
        <td><font size="2" face="Times New Roman">D{{$i+1}}</font></td>
        </tr>
      @endfor
      <tr>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>Jumlah</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>{{ $kredit }}</b></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
      </tr>
    </table>
    <div class="column">
      <font size="2" face="Times New Roman">Demikianlah pernyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</font>
    </div>
    <div class="column">
      <div style="text-align: right;">
        <font size="2" face="Times New Roman"> Jakarta, {{ $date->toFormattedDateString() }}</font><br>
        <font size="2" face="Times New Roman">Ketua STMIK Swadharma</font><br>
        <br>
        <br>
        <br>
        <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
      </div>
    </div>
    </div>
  </body>
</html>

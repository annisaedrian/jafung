@extends('layouts.app')
@section('content')
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="columns">
          <div class="column">
            <p class="card-header-title">
              Tambah Data Penunjang
            </p>
          </div>
        </div>
      </div>
      <div class="card-content">

        @isset($data)
        <form class="form-vertical" action="{{ route('penunjang.store') }}" method="post">
              {{ csrf_field() }}
        <div class="columns">
          <div class="column">
            <div class="field">
             <label class="label">Nama Dosen</label>
             <div class="control">
                  <input type="text" name="name" class="input" value="{{$data[0]->name}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">Penanggung Jawab</label>
             <div class="control">
                  <input type="text" class="input" value="{{$a[0]->name}}" readonly>
                  <input type="hidden" name="user_id" class="input" value="{{$a[0]->id}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">NIP</label>
             <div class="control">
                  <input type="text" name="nip" class="input" value="{{$data[0]->nip}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">NIDN</label>
             <div class="control">
                  <input type="text" name="nidn" class="input" value="{{$data[0]->nidn}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">Pangkat</label>
             <div class="control">
                  <input type="text" name="pangkat" class="input" value="{{$data[0]->pangkat}}" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="columns">
          <div class="column">
            <div class="field">
             <label class="label">Gol. Ruang</label>
             <div class="control">
                  <input type="text" name="gol_ruang" class="input" value="{{$data[0]->gol_ruang}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">Jabatan Fungsional</label>
             <div class="control">
                  <input type="text" name="jab_fungsional" class="input" value="{{$data[0]->jab_fungsional}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">TMT</label>
             <div class="control">
                  <input type="text" name="tmt" class="input" value="{{$data[0]->tmt}}" readonly>
              </div>
            </div>
          </div>
          <div class="column">
            <div class="field">
             <label class="label">Unit Kerja</label>
             <div class="control">
                  <input type="text" name="unit_kerja" class="input" value="{{$data[0]->unit_kerja}}" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="notification is-info"> Pastikan Semua Data Terisi! </div>
          <div class="card">
            <header class="card-header">
              <p class="card-header-title">
                Kegiatan Penunjang Tugas Pokok Dosen
              </p>
              <a href="#" id="plus" class="card-header-icon" aria-label="more options">
                <span class="icon">
                  <i class="fas fa-plus" aria-hidden="true"></i>
                </span>
              </a>
              <a href="#" id="minus" class="card-header-icon" aria-label="remove options">
                <span class="icon">
                  <i class="fas fa-minus" aria-hidden="true"></i>
                </span>
              </a>
            </header>
            <div class="card-content parent">
              <div class="column child">
                <div class="columns">
                  <div class="column">
                    <div class="field">
                      <label class="label">Kegiatan Penunjang Tri Dharma PT</label>
                      <div class="control">
                        <textarea class="textarea" type="text" name="kegiatan_penunjang[]"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="columns">
                  <div class="column">
                    <div class="field">
                      <label class="label">Kedudukan / Tingkat</label>
                      <div class="control">
                        <input class="input" type="text" name="kedudukan[]">
                      </div>
                    </div>
                  </div>
                  <div class="column">
                    <div class="field">
                      <label class="label">Tempat / Instansi</label>
                      <div class="control">
                        <input class="input" type="text" name="tempat[]">
                      </div>
                    </div>
                  </div>
                  <div class="column">
                    <div class="field">
                      <label class="label">Tanggal</label>
                      <div class="control">
                        <input class="input" type="text" name="tgl[]">
                      </div>
                    </div>
                  </div>
                    <div class="column">
                    <div class="field">
                      <label class="label">Jumlah Angka Kredit</label>
                      <div class="control">
                        <input class="input" type="text" name="jml_kredit[]">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <div class="columns m-t-30 is-gapless">
                <div class="column">
                  <a href="{{ route('penunjang.index') }}"class="button is-danger">Cancel</a>
                </div>
                <div class="column">
                  <button class="button is-success" type="submit">Save</button>
                </div>
              </div>
            </form>
        @else
          <form class="form-vertical" action="{{ url('/penunjang/find') }}" method="post">
              {{ csrf_field() }}
              <div class="field">
                <label class="label">Nama Dosen</label>
                  <div class="control">
                  <select class="input" type="text" name="name" id="dosen"></select>
                  </div>
                </div>
                <div class="column">
                  <button class="button is-success" type="submit">Submit</button>
                </div>
          </form>
        @endisset
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">
  $('#dosen').select2({
    placeholder: 'Cari',
    ajax: {
      url: '/api/dosen/search',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.name
            }
          })
        };
      },
      cache: true
    }
  });
  $('#minus').hide();
  $('#plus').click(function(){
      $('#minus').show();
      $('.child:last').clone().appendTo('.parent');
    });
    $('#minus').click(function(){
      if ($(".child").length == 2) {
        $('.child:last').remove();
        $('#minus').hide();
      } else {
        $('.child:last').remove();
      }
    });
  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

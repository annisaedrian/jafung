@extends('layouts.app')
@section('content')
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="columns">
          <div class="column">
            <p class="card-header-title">
              Edit Data Pengabdian
            </p>
          </div>
        </div>
      </div>
      <div class="notification is-info"> Pastikan Semua Data Terisi! </div>
      <div class="card-content">
        <form class="form-vertical" action="{{ url('detail_pengabdian/'.$data->id) }}" method="post">
          {{ csrf_field() }}
          @method('PUT')
          <input type="hidden" name="_method" value="PUT">
            <div class="columns">
              <div class="column">
                <div class="field">
                  <label class="label">Nama Dosen</label>
                  <div class="control">
                    <input type="text" name="name" class="input" value="{{$data->name}}" readonly>
                  </div>
                </div>
              </div>
              <div class="column">
                <div class="field">
                  <label class="label">Jabatan Fungsional</label>
                  <div class="control">
                    <input type="text" name="jab_fungsional" class="input" value="{{$data->jab_fungsional}}" readonly>
                  </div>
                </div>
              </div>
            </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Kegiatan Pengabdian Kepada Masyarakat</label>
                <div class="control">
                <textarea class="textarea" type="text" name="kegiatan_pengabdian">{{ $data->kegiatan_pengabdian }}</textarea>
              </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Bentuk</label>
                <div class="control">
                  <input class="input" type="text" value="{{ $data->bentuk }}" name="bentuk">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Tempat / Instansi</label>
                <div class="control">
                  <input class="input" type="text" value="{{ $data->tempat }}" name="tempat">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Tanggal</label>
                <div class="control">
                  <input class="input" type="text" value="{{ $data->tgl }}" name="tgl">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jumlah Angka Kredit</label>
                <div class="control">
                  <input class="input" type="text" value="{{ $data->jml_kredit }}" name="jml_kredit">
                </div>
              </div>
            </div>
          </div>
          <div class="columns m-t-30 is-gapless">
            <div class="column">
              <a href="{{ route('pengabdian.index') }}"class="button is-danger">Cancel</a>
            </div>
            <div class="column">
              <button class="button is-success" type="submit">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script type="text/javascript">

  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

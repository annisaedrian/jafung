@extends('layouts.app')

@section('content')
<div class="column">
    <div class="card">
        <div class="card-header">
                    <p class="card-header-title">
                    Data Dosen
                    </p>
          <div class="p-t-5 p-r-5">
            <a href="{{ URL::to('dosen/excel/xls') }}"class="button is-link">Download Excel XLS </a>
            <a href="{{ URL::to('dosen/excel/xlsx') }}"class="button is-info p-l-10">Download Excel XLSX </a>
            <a href="{{ URL::to('dosen/excel/csv') }}"class="button is-link p-l-10">Download Excel CSV </a>
          </div>
        </div>

        <div class="card-content table__wrapper">
        <div class="columns">
          <a href="{{ route('dosen.create') }}"class="button is-primary">Create</a>
        </div>
          <table class="table is-bordered pricing__table" id="datatable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>NIP / NIDN</th>
                <th>Pangkat / Golongan Ruang</th>
                <th>Jabatan Fungsional</th>
                <th>Unit Kerja</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($data as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->name }}</td>
                <td>{{ $a->nip }} / {{ $a->nidn }} </td>
                <td>{{ $a->pangkat }} / {{ $a->gol_ruang }} </td>
                <td>{{ $a->jab_fungsional }}</td>
                <td>{{ $a->unit_kerja}}</td>
                <td>
                  <div class="columns">
                    <div class="column">
                      <a href="dosen/{{$a->id}}" class="button is-info">View</a>
                    </div>
                    <div class="column">
                      <form action="dosen/{{$a->id}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                        <button type="submit" name="button" class="button is-danger">Delete</button>
                      </form>
                    </div>
                  </div>
                </td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $('#datatable').DataTable();
  </script>
@endpush

@push('styles')
  <style media="screen">
    .pricing__table {
        width: 100%;
        overflow-x: auto;
    }

    .table__wrapper {
        overflow-x: auto;
    }
  </style>
@endpush

@extends('layouts.app')
@section('content')
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="columns">
          <div class="column">
            <p class="card-header-title">
              View Data Dosen
            </p>
          </div>
        </div>
      </div>

      <div class="card-content">
        <form class="form-vertical" action="{{ url('dosen/'.$data->id) }}" method="PUT">
          @csrf
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Nama Dosen</label>
                <div class="control">
                  <input type="text" name="name" class="input" value="{{$data->name}}" readonly>
                </div>
              </div>
            </div>
            @for($i = 0; $i < count($a); $i++)
              @if($a[$i]->name)
                <div class="column">
                  <div class="field">
                    <label class="label">Penanggung Jawab</label>
                    <div class="control">
                      <input class="input" type="text" name="user_id" value="{{$a[$i]->name}}" readonly>
                    </div>
                  </div>
                </div>
              @else

              @endif
            @endfor
            <div class="column">
              <div class="field">
                <label class="label">Email</label>
                <div class="control">
                  <input class="input" type="text" name="email" value="{{$data->email}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">NIP</label>
                <div class="control">
                  <input class="input" type="text" name="nip" value="{{$data->nip}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">NIDN</label>
                <div class="control">
                  <input class="input" type="text" name="nidn" value="{{$data->nidn}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Nomor Seri KARPEG</label>
                <div class="control">
                  <input type="text" name="no_karpeg" class="input" value="{{$data->no_karpeg}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Tempat dan Tanggal Lahir</label>
                <div class="control">
                  <input class="input" type="text" name="ttl" value="{{$data->ttl}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jenis Kelamin</label>
                <div class="control">
                  <input class="input" type="text" name="jk" value="{{$data->jk}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Pendidikan Tertinggi</label>
                <div class="control">
                  <input type="text" name="pendidikan" class="input" value="{{$data->pendidikan}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Pangkat</label>
                <div class="control">
                  <input class="input" type="text" name="pangkat" value="{{$data->pangkat}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Golongan Ruang</label>
                <div class="control">
                  <input class="input" type="text" name="gol_ruang" value="{{$data->gol_ruang}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jabatan Fungsional</label>
                <div class="control">
                  <input type="text" name="jab_fungsional" class="input" value="{{$data->jab_fungsional}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">TMT</label>
                <div class="control">
                  <input class="input" type="text" name="tmt" value="{{$data->tmt}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Fakultas</label>
                <div class="control">
                  <input class="input" type="text" name="fakultas" value="{{$data->fakultas}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jurusan</label>
                <div class="control">
                  <input type="text" class="input" name="jurusan" value="{{$data->jurusan}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Masa Kerja</label>
                <div class="control">
                  <input class="input" type="text" name="masa_kerja" value="{{$data->masa_kerja}}" readonly>
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Unit Kerja</label>
                <div class="control">
                  <input class="input" type="text" name="unit_kerja" value="{{$data->unit_kerja}}" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
          <div class="column">
              <a href="{{ route('dosen.index') }}" class="button is-primary">Back</a>
            </div>
            <div class="column">
              <a href="{{ url('dosen/'.$data->id).'/edit' }}" class="button is-info">Edit</a>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>

    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">

  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

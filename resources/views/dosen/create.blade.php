@extends('layouts.app')
@section('content')
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="columns">
          <div class="column">
            <p class="card-header-title">
              Tambah Data Dosen
            </p>
          </div>
        </div>
      </div>
      <div class="notification is-info"> Pastikan Semua Data Terisi</div>
      <div class="card-content">
        <form class="form-vertical" action="{{ route('dosen.store') }}" method="post">
          {{ csrf_field() }}
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Nama
                  <td><font color="red" class="is-subtitle"size="2">(*)</font></td>
                </label>
                <div class="control">
                  <input class="input" type="text" name="name">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Email
                  <td><font color="red" class="is-subtitle"size="2">(*)</font></td>
                </label>
                <div class="control">
                  <input class="input" type="text" name="email">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Password
                  <td><font class="is-subtitle"size="2">( Password dapat diubah melalui admin )</font></td>
                </label>
                <div class="control">
                  <input class="input" type="text" name="password" placeholder="Default Password : password" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">NIP</label>
                <div class="control">
                  <input class="input" type="text" name="nip">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">NIDN</label>
                <div class="control">
                  <input class="input" type="text" name="nidn">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Nomor Seri KARPEG</label>
                <div class="control">
                   <input class="input" type="text" name="no_karpeg">
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Tempat dan Tanggal Lahir</label>
                <div class="control">
                  <input class="input" type="text" name="ttl">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jenis Kelamin <font color="red" class="is-subtitle"size="2">(*)</font></label>
                <div class="control">
                  <div class="select">
                    <select name="jk" type="enum">
                      <option value="Jenis Kelamin">Jenis Kelamin</option>
                      <option value="Laki - Laki">Laki - Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
              <div class="column">
                <div class="field">
                  <label class="label">Pendidikan Tertinggi</label>
                  <div class="control">
                    <input class="input" type="text" name="pendidikan">
                  </div>
                </div>
              </div>
            </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Pangkat</label>
                <div class="control">
                  <input class="input" type="text" name="pangkat">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Golongan Ruang</label>
                <div class="control">
                  <input class="input" type="text" name="gol_ruang">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jabatan Fungsional
                  <td><font color="red" class="is-subtitle"size="2">(*)</font></td>
                </label>
                <div class="control">
                  <div class="select">
                    <select name="jab_fungsional" type="enum">
                      <option value="Jabatan Fungsional">Jabatan Fungsional</option>
                      <option value="Asisten Ahli">Asisten Ahli</option>
                      <option value="Lektor">Lektor</option>
                      <option value="Lektor Kepala">Lektor Kepala</option>
                      <option value="Profesor">Profesor</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">TMT</label>
                <div class="control">
                  <input class="input" type="text" name="tmt">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Fakultas</label>
                <div class="control">
                  <input class="input" type="text" name="fakultas">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Jurusan</label>
                <div class="control">
                <input class="input" type="text" name="jurusan">
                </div>
              </div>
            </div>
          </div>
          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Masa Kerja</label>
                <div class="control">
                  <input class="input" type="text" name="masa_kerja">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Unit Kerja</label>
                <div class="control">
                  <input class="input" type="text" name="unit_kerja">
                </div>
              </div>
            </div>
            <div class="column">
              <div class="field">
                <label class="label">Penanggung Jawab
                  <td><font color="red" class="is-subtitle"size="2">(*)</font></td>
                </label>
                  <div class="control">
                  <select class="input" type="text" name="penanggung_jawab" id="dosen"></select>
                  </div>

                  <td><font color="red" class="is-subtitle"size="2">Data Penanggung Jawab dapat diisi melalui form dosen</font></td>
              </div>
            </div>
          </div>
              <div class="columns m-t-30 is-gapless">
                <div class="column">
                  <a href="{{ route('dosen.index') }}"class="button is-danger">Cancel</a>
                </div>
                <div class="column">
                  <button class="button is-success" type="submit">Save</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">
 $('#dosen').select2({
    placeholder: 'Cari',
    ajax: {
      url: '/api/dosen/search',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });
  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

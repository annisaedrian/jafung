@role(['superadministrator','admin', 'dosen'])
  <aside class="column is-2 aside is-hidden-mobile is-clearfix">
  <nav class="menu">
    <p class="menu-label"></p>
    <ul class="menu-list is-tab">
      <li><a href="{{ url('/') }}"><span class="icon is-small"><i class="fas fa-home"></i></span> Home</a></li>
    </ul>
    @role(['superadministrator','admin'])
    <p class="menu-label"> General</p>
    <ul class="menu-list is-tab">
      <li><a href="{{ route('dosen.index') }}"><span class="icon is-small"><i class="fas fa-users"></i></span> Dosen</a></li>
    </ul>
    @endrole

    <p class="menu-label">
      Master
    </p>
    <ul class="menu-list is-tab">
      <li>
      <li><a href="{{ route('pendidikan.index') }}"><span class="icon is-small"><i class="fas fa-bookmark"></i></span> Pendidikan</a>
        @role(['superadministrator','admin'])
      <ul>
      <li><a href="{{ route('unsurpendidikan.index') }}">Unsur Pendidikan</a></li>
      <li><a href="{{ route('pelaksanapendidikan.index') }}">Unsur Pelaksanaan Pendidikan</a></li>
      </ul>
      @endrole
      </li>
      <li><a href="{{ route('penelitian.index') }}"><span class="icon is-small"><i class="fas fa-bookmark"></i></span>  Penelitian</a></li>
      <li><a href="{{ route('pengabdian.index') }}"><span class="icon is-small"><i class="fas fa-bookmark"></i></span>  Pengabdian</a></li>
      <li><a href="{{ route('penunjang.index') }}"><span class="icon is-small"><i class="fas fa-bookmark"></i></span>  Penunjang</a></li>
      <li><a href="{{ route('dupak.index') }}"><span class="icon is-small"><i class="fas fa-bookmark"></i></span> Dupak</a></li>
      </li>
    </ul>

    <!-- @role('superadministrator')
      <p class="menu-label">
        Role Settings
      </p>
      <ul class="menu-list is-tab">
        <li><a href=""><span class="icon is-small"><i class="fas fa-users"></i></span> Management</a></li>
      </ul>
    @endrole -->
  </nav>
</aside>
@endrole

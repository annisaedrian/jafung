@extends('layouts.app')
@section('content')
<div class="column">
    <div class="card">
        <div class="card-header">
          <p class="card-header-title">
            Kegiatan Unsur Pendidikan
          </p>
        </div>

      <div class="card-content">
        <form class="form-vertical" action="{{ url('unsurpendidikan/'.$data->id) }}" method="PUT">
          @csrf
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Nama Dosen</label>
                        <div class="control">
                            <input type="text" name="name" class="input" value="{{$data->name}}" readonly>
                        </div>
                    </div>
                </div>
                @for($i = 0; $i < count($a); $i++)
                  @if($a[$i]->name)
                    <div class="column">
                      <div class="field">
                        <label class="label">Penanggung Jawab</label>
                        <div class="control">
                          <input class="input" type="text" name="user_id" value="{{$a[$i]->name}}" readonly>
                        </div>
                      </div>
                    </div>
                  @else

                  @endif
                @endfor
                <div class="column">
                    <div class="field">
                        <label class="label">NIP / NIDN</label>
                        <div class="control">
                            <input class="input" type="text" value="{{$data->nip}} , {{$data->nidn}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Pangkat / Golongan Ruang</label>
                        <div class="control">
                            <input class="input" type="text" value="{{$data->pangkat}} , {{$data->gol_ruang}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Jabatan Fungsional / TMT</label>
                        <div class="control">
                            <input type="text" class="input" value="{{$data->jab_fungsional}} , {{$data->tmt}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Unit Kerja</label>
                        <div class="control">
                            <input class="input" type="text" name="unit_kerja" value="{{$data->unit_kerja}}" readonly>
                        </div>
                    </div>
                </div>
          </div>

            <div class="notification is-info"> Kegiatan Pendidikan dan Pengajaran</div>
            <table class="table is-bordered pricing__table" id="datatable">
            <thead>
              <tr>
                <th>No</th>
                <th>Kegiatan Pendidikan dan Pengajaran</th>
                <th>Tempat / Instansi</th>
                <th>Tanggal</th>
                <th>Jumlah Angka Kredit</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($data->detail_unsurpendidikan as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->kegiatan_pend }}</td>
                <td>{{ $a->tempat }}</td>
                <td>{{ $a->tgl }}</td>
                <td>{{ $a->jml_kredit }}</td>
                <td>
                  <div class="columns">
                    <div class="column">
                        <a href="{!! route('unsurpendidikan.edit', $a->id) !!}" class="button is-info">Edit</a>
                    </div>
                  </div>
                </td>
              </tr>

              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>


            </div>
            </div>
        </form>
      </div>
    </div>
</div>

    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $('#datatable').DataTable();
  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

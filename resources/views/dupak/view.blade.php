@extends('layouts.app')
@section('content')
  <div class="column">
    <div class="card">
      <div class="card-header">
            <p class="card-header-title">
              View Data Penetapan Angka Kredit Jabatan Fungsional Dosen
            </p>
              @role(['superadministrator','admin'])
            <div class="p-t-5 p-r-5">
              <a href="{{action('DupakController@downloadPDF', $unsur[0]->name)}}"class="button is-link">Download PDF </a>
            </div>
            @endrole
        </div>
      </div>

      <div class="card-content">
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Nama Dosen</label>
                        <div class="control">
                            <input type="text" name="name" class="input" value="{{$unsur[0]->name}}" readonly>
                        </div>
                    </div>
                </div>
                    <div class="column">
                      <div class="field">
                        <label class="label">Penanggung Jawab</label>
                        <div class="control">
                          <input class="input" type="text" name="user_id" value="{{$a[0]->name}}" readonly>
                        </div>
                      </div>
                    </div>
                <div class="column">
                    <div class="field">
                        <label class="label">NIP / NIDN</label>
                        <div class="control">
                            <input class="input" type="text" value="{{$unsur[0]->nip}} , {{$unsur[0]->nidn}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Pangkat / Golongan Ruang</label>
                        <div class="control">
                            <input class="input" type="text" value="{{$unsur[0]->pangkat}} , {{$unsur[0]->gol_ruang}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Jabatan Fungsional / TMT</label>
                        <div class="control">
                            <input type="text" class="input" value="{{$unsur[0]->jab_fungsional}} , {{$unsur[0]->tmt}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Unit Kerja</label>
                        <div class="control">
                            <input class="input" type="text" name="unit_kerja" value="{{$unsur[0]->unit_kerja}}" readonly>
                        </div>
                    </div>
                </div>
          </div>

          <div class="notification is-info">Unsur Pendidikan</div>
            <table class="table is-bordered pricing__table">
            <thead>
              <tr>
                <th>No</th>
                <th>Kegiatan Pendidikan dan Pengajaran</th>
                <th>Tempat / Instasi</th>
                <th>Tanggal</th>
                <th>Jumlah Angka Kredit</th>
                <th>Ket. / Bukti Fisik</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($unsur[0]->detail_unsurpendidikan as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->kegiatan_pend }}</td>
                <td>{{ $a->tempat }}</td>
                <td>{{ $a->tgl }}</td>
                <td>{{ $a->jml_kredit }}</td>
                <td>A{{ $n }}</td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
          <div class="notification is-info">Unsur Pelaksanaan Pendidikan</div>
            <table class="table is-bordered pricing__table">
            <thead>
              <tr>
                <th>No</th>
                <th>Kegiatan Pendidikan dan Pengajaran</th>
                <th>Tempat / Instasi</th>
                <th>Tanggal</th>
                <th>Jumlah Angka Kredit</th>
                <th>Ket. / Bukti Fisik</th>
              </tr>
            </thead>
            <tbody>
              @foreach($pelaksana[0]->detail_pelaksanapendidikan as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->kegiatan_pend }}</td>
                <td>{{ $a->tempat }}</td>
                <td>{{ $a->tgl }}</td>
                <td>{{ $a->jml_kredit }}</td>
                <td>A{{ $n }}</td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
          <div class="notification is-info">Penelitian</div>
            <table class="table is-bordered pricing__table">
            <thead>
              <tr>
                <th>No</th>
                <th>Judul Karya Ilmiah</th>
                <th>Nilai Angka Kredit</th>
                <th>Tim Penilai Perguruan Tinggi</th>
                <th>Tim Penilai Pusat</th>
                <th>Ket. / Bukti Fisik</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($penelitian[0]->detail_penelitian as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->judul_karya }}</td>
                <td>{{ $a->nilai_kredit }}</td>
                <td>{{ $a->penilai_perguruan }}</td>
                <td>{{ $a->penilai_pusat }}</td>
                <td>B{{ $n }}</td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
          <div class="notification is-info">Pengabdian</div>
            <table class="table is-bordered pricing__table">
            <thead>
              <tr>
                <th>No</th>
                <th>Kegiatan Pengabdian Kepada Masyarakat</th>
                <th>Bentuk</th>
                <th>Tempat / Instasi</th>
                <th>Tanggal</th>
                <th>Jumlah Angka Kredit</th>
                <th>Ket. / Bukti Fisik</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($pengabdian[0]->detail_pengabdian as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->kegiatan_pengabdian }}</td>
                <td>{{ $a->bentuk }}</td>
                <td>{{ $a->tempat }}</td>
                <td>{{ $a->tgl }}</td>
                <td>{{ $a->jml_kredit}}</td>
                <td>C{{ $n }}</td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
          <div class="notification is-info">Penunjang</div>
            <table class="table is-bordered pricing__table">
            <thead>
              <tr>
                <th>No</th>
                <th>Kegiatan Penunjang Tri Dharma PT</th>
                <th>Kedudukan / Tingkat</th>
                <th>Tempat / Instasi</th>
                <th>Tanggal</th>
                <th>Jumlah Angka Kredit</th>
                <th>Ket. / Bukti Fisik</th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1 ?>
              @foreach($penunjang[0]->detail_penunjang as $a)
              <tr>
                <th>{{ $n }}</th>
                <td>{{ $a->kegiatan_penunjang }}</td>
                <td>{{ $a->kedudukan }}</td>
                <td>{{ $a->tempat }}</td>
                <td>{{ $a->tgl }}</td>
                <td>{{ $a->jml_kredit}}</td>
                <td>D{{ $n }}</td>
              </tr>
              <?php $n++ ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $('#datatable').DataTable();
  </script>
@endpush

@push('style')
  <style media="screen">

  </style>
@endpush

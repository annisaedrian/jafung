<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Penetapan Angka Kredit - {{ $unsur[0]->name }}</title>
  </head>
  <body>
    <div class="p-t-50 p-b-50 p-l-50 p-r-50">
      <center><font size="3" face="Times New Roman"><b>DAFTAR USUL PENETAPAN ANGKA KREDIT</b></font></center>
      <center><font size="3" face="Times New Roman"><b>JABATAN FUNGSIONAL DOSEN</b></font></center><br>
      <center><font size="2" face="Times New Roman"><b>Tanggal Penilaian :</b></font></center><br>

      <table class="table is-bordered is-striped is-narrow is-fullwidth">
        <tr>
          <td rowspan="7">
            <font size="2" face="Times New Roman"><b>I</b></font>
          </td>
            <td colspan="2" >
              <center><font size="2" face="Times New Roman"><b> KETERANGAN PERORANGAN</b></font></center>
            </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">N a m a </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">N I P / NIDN </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->nip}} , {{$a[0]->nidn}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">Nomor Seri KARPEG</font>
          </td>
          <td>
            <font size="2" face="Times New Roman"></font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">Tempat dan Tanggal Lahir</font>
          </td>
          <td>
            <font size="2" face="Times New Roman"></font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">Jenis Kelamin</font>
          </td>
          <td>
            <font size="2" face="Times New Roman"></font>
          </td>
        </tr>
        <tr>
           <td>
             <font size="2" face="Times New Roman">Pendidikan Tertinggi</font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
        <tr>
           <td rowspan="5"></td>
           <td>
             <font size="2" face="Times New Roman">Pangkat dan Golongan Ruang/TMT</font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
        <tr>
           <td>
             <font size="2" face="Times New Roman">Jabatan Fungsional/TMT</font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
        <tr>
           <td>
             <font size="2" face="Times New Roman">Fakultas/Jurusan</font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
        <tr>
           <td>
             <font size="2" face="Times New Roman">Masa Kerja </font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
        <tr>
           <td>
             <font size="2" face="Times New Roman">Unit Kerja </font>
           </td>
           <td>
             <font size="2" face="Times New Roman"></font>
           </td>
         </tr>
       </table>
    <table class="table is-bordered is-striped is-narrow is-fullwidth">
      <tr>
        <td rowspan="">
          <font size="2" face="Times New Roman"><b>II</b></font>
        </td>
          <td colspan="7" >
            <center><font size="2" face="Times New Roman"><b>UNSUR YANG DINILAI</b></font></center>
          </td>
      </tr>
      <link rel="stylesheet" href="{{ public_path('css/app.css') }}">
      <tr>
        <td rowspan="3">
          <font size="2" face="Times New Roman">No  </font>
        </td>
        <td rowspan="3">
          <font size="2" face="Times New Roman">Unsur dan Sub. Unsur</font>
        </td>
        <td colspan="6">
          <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">PT/Kopertis Pengusul</font></center>
        </td>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">Tim Penilai</font></center>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
      </tr>
      {{-- unsur pendidikan --}}
      <tr>
        <td>
          <font size="2" face="Times New Roman"> <b>I</b> </font>
        </td>
        <td>
          <font size="2" face="Times New Roman"> <b>UNSUR UTAMA :<br> PENDIDIKAN</b> </font>
        </td>
        <td colspan="6"></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">a. Mengikuti pendidikan sekolah dari dan memperoleh gelar/sebutan/ijazah/akta</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      @for ($i = 0; $i < count($unsur[0]->detail_unsurpendidikan); $i++)
        <tr>
          <td></td>
        <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $unsur[0]->detail_unsurpendidikan[$i]->kegiatan_pend }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $unsur[0]->detail_unsurpendidikan[$i]->jml_kredit }}</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      @endfor
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">b. Mengikuti pendidikan sekolah dan memperoleh gelar/sebutan/ijazah/akta tambahan yang setingkat atau lebih tinggi di luar bidang ilmunya</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">c. Mengikuti pendidikan dan pelatihan fungsional Dosen dan memperoleh Surat Tanda Tamat Pendidikan dan Pelatihan</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td style="text-align: right;" ><font size="2" face="Times New Roman"> <b>Jumlah</b> </font></td>
        <td><font size="2" face="Times New Roman"> <b>{{$kredit_unsur}}</b> </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
        {{-- pelaksana pendidikan --}}
      <tr>
        <td></td>
        <td colspan="7" >
        <font size="2" face="Times New Roman"><b>TRIDHARMA PERGURUAN TINGGI</b></font>
        </td>
      </tr>
      <tr>
        <td rowspan="3">
          <font size="2" face="Times New Roman">No  </font>
        </td>
        <td rowspan="3">
          <font size="2" face="Times New Roman">Unsur dan Sub. Unsur</font>
        </td>
        <td colspan="6">
          <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">PT/Kopertis Pengusul</font></center>
        </td>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">Tim Penilai</font></center>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman"> <b></b> </font>
        </td>
        <td>
          <font size="2" face="Times New Roman"> <b>A. MELAKSANAKAN PENDIDIKAN DAN PENGAJARAN</b> </font>
        </td>
        <td colspan="6"></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(1)
          Melaksanakan perkuliah/tutorial dan membimbing menguji serta menyelenggarakan pendidikan di laboratiun,
          praktek keguruan, bekel/studio/kebun/percobaan/teknologi pengajaran dan praktek lapangan
        </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      @for ($i = 0; $i < count($pelaksana[0]->detail_pelaksanapendidikan); $i++)
        <tr>
          <td></td>
        <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $pelaksana[0]->detail_pelaksanapendidikan[$i]->kegiatan_pend }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $pelaksana[0]->detail_pelaksanapendidikan[$i]->jml_kredit }}</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      @endfor
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(2)
          Membimbing Seminar Mahawasiswa
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(3)
          Membimbing Kuliah Kerja Nyata (KKN), Praktek Kerja Nyata (PKN), Praktek Kerja Lapangan (PKL)
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(4)
            Membimbing dan ikut membimbing dalam menghasilkan laporan akhir studi/skripsi tesis/disertasi
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(5)
          Bertugas sebagai penguji pada Ujian Akhir
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(6)
          Membina kegiatan mahasiswa dibidang Akademik dan Kemahasiswaan
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(7)
          Mengembangkan program kuliah : diktat, buku ajar
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(8)
          Mengembangkan bahan pengajaran (modul)
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(9)
          Menyampaikan orasi ilmiah
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(10)
          Menduduki jabatan Pimpinan Perguruan Tinggi
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(11)
          Membimbing dosen yang lebih rendah jabatan fungsional
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(12)
          Melaksanakan kegiatan detasering dan pencangkokan dosen
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td style="text-align: right;" ><font size="2" face="Times New Roman"> <b>Jumlah</b> </font></td>
        <td><font size="2" face="Times New Roman"> <b>{{$kredit_pelaksana}}</b> </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      {{-- penelitian --}}
      <tr>
        <td rowspan="3">
          <font size="2" face="Times New Roman">No  </font>
        </td>
        <td rowspan="3">
          <font size="2" face="Times New Roman">Unsur dan Sub. Unsur</font>
        </td>
        <td colspan="6">
          <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">PT/Kopertis Pengusul</font></center>
        </td>
        <td colspan="3">
          <center><font size="2" face="Times New Roman">Tim Penilai</font></center>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Lama</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Baru</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah</font>
        </td>
      </tr>
      <tr>
        <td>
          <font size="2" face="Times New Roman"> <b></b> </font>
        </td>
        <td>
          <font size="2" face="Times New Roman"> <b>B. MELAKSANAKAN PENELITIAN</b> </font>
        </td>
        <td colspan="6"></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(1)
          Menghasilkan Karya Ilmiah
        </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      {{-- @for ($i = 0; $i < count($penelitian->detail_penelitian); $i++)
        <tr>
          <td></td>
        <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $penelitian->detail_penelitian[$i]->judul_karya }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $penelitian->detail_penelitian[$i]->nilai_kredit }}</font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      @endfor --}}
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(2)
          Menerjemahkan/menyadur buku ilmiah
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(3)
          Mengedit/menyunting Karya Ilmiah
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(4)
          Membuat rancangan dan karya teknologi, yang dipatenkan
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Times New Roman">(5)
          Membuat rancangan dan karya teknologi rancangan dan karya seni monumental/seni pertunjukan/karya sastra
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
      </tr>
      <tr>
        <td></td>
        <td style="text-align: right;" ><font size="2" face="Times New Roman"> <b>Jumlah</b> </font></td>
        <td><font size="2" face="Times New Roman"> <b></b> </font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      {{-- pengabdian --}}
      <tr>
          <td rowspan="3">
            <font size="2" face="Times New Roman">No  </font>
          </td>
          <td rowspan="3">
            <font size="2" face="Times New Roman">Unsur dan Sub. Unsur</font>
          </td>
          <td colspan="6">
            <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
          </td>
        </tr>
      <tr>
          <td colspan="3">
            <center><font size="2" face="Times New Roman">PT/Kopertis Pengusul</font></center>
          </td>
          <td colspan="3">
            <center><font size="2" face="Times New Roman">Tim Penilai</font></center>
          </td>
        </tr>
      <tr>
          <td>
            <font size="2" face="Times New Roman">Lama</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Baru</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Jumlah</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Lama</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Baru</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Jumlah</font>
          </td>
        </tr>
      <tr>
          <td>
            <font size="2" face="Times New Roman"> <b></b> </font>
          </td>
          <td>
            <font size="2" face="Times New Roman"> <b>C. MELAKSANAKAN PPENGABDIAN <br>KEPADA MASYARAKAT </b> </font>
          </td>
          <td colspan="6"></td>
        </tr>
      <tr>
          <td></td>
          <td><font size="2" face="Times New Roman">(1)
            Menduduki jabatan pimpinan dan lemabaga pemerintah/pejabat negara yang harus dibebaskan dari jabatan organiknya
          </font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      <tr>
          <td></td>
          <td><font size="2" face="Times New Roman">(2)
            Melaksanakan pengembangan hasil pendidikan dan penelitian yang dapat dimanfaatkan oleh masyarakat
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      <tr>
          <td></td>
          <td><font size="2" face="Times New Roman">(3)
            Memberikan latihan/penyuluh penataran/ceramah pada masyarakat
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
        </tr>
        {{-- @for ($i = 0; $i < count($penelitian->detail_penelitian); $i++)
            <tr>
              <td></td>
            <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $penelitian->detail_penelitian[$i]->judul_karya }}</font></td>
            <td><font size="2" face="Times New Roman">{{ $penelitian->detail_penelitian[$i]->nilai_kredit }}</font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            </tr>
          @endfor --}}
      <tr>
          <td></td>
          <td><font size="2" face="Times New Roman">(4)
            Memberi pelayanan kepada masyarakat atau kegiatan lain yang menunjang tugas umum pemerintah dan pembangunan
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      <tr>
          <td></td>
          <td><font size="2" face="Times New Roman">(5)
            Membuat/menulis karya pengabdian pada masyarakat yang tidak dipublikasikan
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
        </tr>
      <tr>
          <td></td>
          <td style="text-align: right;" ><font size="2" face="Times New Roman"> <b>Jumlah</b> </font></td>
          <td><font size="2" face="Times New Roman"> <b></b> </font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          <td><font size="2" face="Times New Roman"></font></td>
          </tr>
      {{-- penunjang --}}
      <tr>
            <td rowspan="3">
              <font size="2" face="Times New Roman">No  </font>
            </td>
            <td rowspan="3">
              <font size="2" face="Times New Roman">Unsur dan Sub. Unsur</font>
            </td>
            <td colspan="6">
              <center><font size="2" face="Times New Roman">Angka Kredit Menurut</font></center>
            </td>
          </tr>
      <tr>
            <td colspan="3">
              <center><font size="2" face="Times New Roman">PT/Kopertis Pengusul</font></center>
            </td>
            <td colspan="3">
              <center><font size="2" face="Times New Roman">Tim Penilai</font></center>
            </td>
          </tr>
      <tr>
            <td>
              <font size="2" face="Times New Roman">Lama</font>
            </td>
            <td>
              <font size="2" face="Times New Roman">Baru</font>
            </td>
            <td>
              <font size="2" face="Times New Roman">Jumlah</font>
            </td>
            <td>
              <font size="2" face="Times New Roman">Lama</font>
            </td>
            <td>
              <font size="2" face="Times New Roman">Baru</font>
            </td>
            <td>
              <font size="2" face="Times New Roman">Jumlah</font>
            </td>
          </tr>
      <tr>
            <td>
              <font size="2" face="Times New Roman"> <b>II</b> </font>
            </td>
            <td>
              <font size="2" face="Times New Roman"> <b>UNSUR PENUNJANG :<br> PENUNJANG TUGAS POKOK DOSEN</b> </font>
            </td>
            <td colspan="6"></td>
          </tr>
      <tr>
              <td></td>
              <td><font size="2" face="Times New Roman">(1)
                Menjadi anggota dalam sebuah panitia/badan pada Perguruan Tinggi
              </font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
            </tr>
      <tr>
              <td></td>
              <td><font size="2" face="Times New Roman">(2)
                Menjadi anggota panitia/badan pada lembaga pemerintah
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
            </tr>
      <tr>
              <td></td>
              <td><font size="2" face="Times New Roman">(3)
                Menjadi anggota orginasasi profesi
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
            </tr>
            {{-- @for ($i = 0; $i < count($penelitian->detail_penelitian); $i++)
                <tr>
                  <td></td>
                <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $penelitian->detail_penelitian[$i]->judul_karya }}</font></td>
                <td><font size="2" face="Times New Roman">{{ $penelitian->detail_penelitian[$i]->nilai_kredit }}</font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                </tr>
              @endfor --}}
      <tr>
              <td></td>
              <td><font size="2" face="Times New Roman">(4)
                Mewakili Perguruan Tinggi/Lembaga Pemerintah duduk dalam panitia antar lembaga
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
            </tr>
      <tr>
              <td></td>
              <td><font size="2" face="Times New Roman">(5)
                Menjadi anggota deligasi nasional ke pertemuan internasional
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
            </tr>
      <tr>
                <td></td>
                <td><font size="2" face="Times New Roman">(6)
                Berperan serta aktif dalam pertemuan ilmiah
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
                <td><font size="2" face="Times New Roman"></font></td>
              </tr>
              {{-- @for ($i = 0; $i < count($penelitian->detail_penelitian); $i++)
                  <tr>
                    <td></td>
                  <td><font size="2" face="Times New Roman">({{$i+1}}) {{ $penelitian->detail_penelitian[$i]->judul_karya }}</font></td>
                  <td><font size="2" face="Times New Roman">{{ $penelitian->detail_penelitian[$i]->nilai_kredit }}</font></td>
                  <td><font size="2" face="Times New Roman"></font></td>
                  <td><font size="2" face="Times New Roman"></font></td>
                  <td><font size="2" face="Times New Roman"></font></td>
                  <td><font size="2" face="Times New Roman"></font></td>
                  <td><font size="2" face="Times New Roman"></font></td>
                  </tr>
                @endfor --}}
      <tr>
            <td></td>
                    <td><font size="2" face="Times New Roman">(7)
                    Mendapat tanda jasa/penghargaan
                    <td><font size="2" face="Times New Roman"></font></td>
                    <td><font size="2" face="Times New Roman"></font></td>
                    <td><font size="2" face="Times New Roman"></font></td>
                    <td><font size="2" face="Times New Roman"></font></td>
                    <td><font size="2" face="Times New Roman"></font></td>
                    <td><font size="2" face="Times New Roman"></font></td>
                  </tr>
      <tr>
            <td></td>
            <td><font size="2" face="Times New Roman">(8)
              Penulis buku pelajaran SLTA kebawah yang diterbitkan dan diedarkan secara nasional
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
          </tr>
      <tr>
            <td></td>
            <td><font size="2" face="Times New Roman">(9)
              Mempunyai prestasi dibidang olahraga/Humaniora
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
            <td><font size="2" face="Times New Roman"></font></td>
          </tr>
      <tr>
              <td></td>
              <td style="text-align: right;" ><font size="2" face="Times New Roman"> <b>Jumlah</b> </font></td>
              <td><font size="2" face="Times New Roman"> <b></b> </font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              <td><font size="2" face="Times New Roman"></font></td>
              </tr>
      {{-- ttd --}}
      <tr style="height:200px;">
        <td></td>
        <td colspan="7"></td>
      </tr>
    </table>

    </div>
  </body>
</html>

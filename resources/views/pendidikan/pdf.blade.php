<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Kegiatan Pendidikan dan Pengajaran - {{ $unsur[0]->name }}</title>
  </head>
  <body>
    <div class="p-t-50 p-b-50 p-l-50 p-r-50">
      <center><font size="3" face="Times New Roman"><b>SURAT PERNYATAAN</b></font></center>
      <center><font size="3" face="Times New Roman"><b>MELAKUKAN KEGIATAN PENDIDIKAN DAN PENGAJARAN</b></font></center><br>
      <div class="column">
        <font size="2" face="Times New Roman">Yang bertanda tangan di bawah ini :</font>
      </div>
      <table class="table is-bordered is-striped is-narrow is-fullwidth">
        <tr>
          <td>
            <font size="2" face="Times New Roman">1.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">N a m a </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">2.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">N I P / NIDN </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->nip}} , {{$a[0]->nidn}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">3.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Pangkat / Gol. Ruang </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->pangkat}} , {{$a[0]->gol_ruang}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">4.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Jabatan Fungsional </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->jab_fungsional}} , {{$a[0]->tmt}} </font>
          </td>
        </tr>
        <tr>
          <td>
            <font size="2" face="Times New Roman">5.</font>
          </td>
          <td>
            <font size="2" face="Times New Roman">Unit Kerja </font>
          </td>
          <td>
            <font size="2" face="Times New Roman">{{$a[0]->unit_kerja}} </font>
          </td>
        </tr>
      </table>
      <div class="column">
        <font size="2" face="Times New Roman">Menyatakan bahwa :</font>
      </div>
       <table class="table is-bordered is-striped is-narrow is-fullwidth">
         <tr>
           <td>
             <font size="2" face="Times New Roman">6.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N a m a </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$unsur[0]->name}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">7.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">N I P / NIDN </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$unsur[0]->nip}} , {{$unsur[0]->nidn}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">8.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Pangkat / Gol. Ruang </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$unsur[0]->pangkat}} , {{$unsur[0]->gol_ruang}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">9.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Jabatan Fungsional </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$unsur[0]->jab_fungsional}} , {{$unsur[0]->tmt}} </font>
           </td>
         </tr>
         <tr>
           <td>
             <font size="2" face="Times New Roman">10.</font>
           </td>
           <td>
             <font size="2" face="Times New Roman">Unit Kerja </font>
           </td>
           <td>
             <font size="2" face="Times New Roman">{{$unsur[0]->unit_kerja}} </font>
           </td>
         </tr>
       </table>
      <div class="column">
        <font size="2" face="Times New Roman">Telah melakukan kegiatan pendidikan dan pengajaran sebagai berikut :</font>
      </div>
      <link rel="stylesheet" href="{{ public_path('css/app.css') }}">
    <table class="table is-bordered is-striped is-narrow is-fullwidth">
      <tr>
        <td>
          <font size="2" face="Times New Roman">No  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Kegiatan Pendidikan dan Pengajaran  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tempat / Instasi</font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Tanggal  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Jumlah Angka Kredit  </font>
        </td>
        <td>
          <font size="2" face="Times New Roman">Ket. / Bukti Fisik  </font>
        </td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman"><b>A</b></font></td>
        <td colspan="5"><font size="2" face="Times New Roman"><b>Unsur Pendidikan </b></font></td>
      </tr>
      @for ($i = 0; $i < count($unsur[0]->detail_unsurpendidikan); $i++)
        <tr>
        <td><font size="2" face="Times New Roman">{{$i+1}}</font></td>
        <td><font size="2" face="Times New Roman">{{ $unsur[0]->detail_unsurpendidikan[$i]->kegiatan_pend }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $unsur[0]->detail_unsurpendidikan[$i]->tempat }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $unsur[0]->detail_unsurpendidikan[$i]->tgl }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $unsur[0]->detail_unsurpendidikan[$i]->jml_kredit }}</font></td>
        <td><font size="2" face="Times New Roman">A{{$i+1}}</font></td>
        </tr>
      @endfor
      <tr>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>Jumlah</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman">{{ $kredit_unsur }}</font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
      </tr>
      {{-- pelaksana pendidikan --}}
      <link rel="stylesheet" href="{{ public_path('css/app.css') }}">
      <tr>
        <td><font size="2" face="Times New Roman"><b>II</b></font></td>
        <td colspan="5"><font size="2" face="Times New Roman"><b>Unsur Pelaksanaan Pendidikan </b></font></td>
      </tr>
      <tr>
        <td><font size="2" face="Times New Roman"><b></b></font></td>
        <td colspan="5"><font size="2" face="Times New Roman">Melaksanakan Perkuliahan</font></td>
      </tr>
      @for ($j = 0; $j < count($pelaksana[0]->detail_pelaksanapendidikan); $j++)
        <tr>
        <td><font size="2" face="Times New Roman">{{$j+1}}</font></td>
        <td><font size="2" face="Times New Roman">{{ $pelaksana[0]->detail_pelaksanapendidikan[$j]->kegiatan_pend }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $pelaksana[0]->detail_pelaksanapendidikan[$j]->tempat }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $pelaksana[0]->detail_pelaksanapendidikan[$j]->tgl }}</font></td>
        <td><font size="2" face="Times New Roman">{{ $pelaksana[0]->detail_pelaksanapendidikan[$j]->jml_kredit }}</font></td>
        <td><font size="2" face="Times New Roman">A{{$i+1}}</font></td>
        </tr>
        <?php $i++ ?>
      @endfor
      <tr>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>Jumlah</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
        <td>
        <font size="2" face="Times New Roman"> <b>{{ $kredit_pelaksana }}</b> </font>
        </td>
        <td>
        <font size="2" face="Times New Roman"></font>
        </td>
      </tr>
    </table>
    <div class="column">
      <font size="2" face="Times New Roman">Demikianlah pernyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</font>
    </div>
    <div class="column">
      <div style="text-align: right;">
        <font size="2" face="Times New Roman"> Jakarta, {{ $date->toFormattedDateString() }}</font><br>
        <font size="2" face="Times New Roman">Ketua STTI NIIT </font><br>
        <br>
        <br>
        <br>
        <font size="2" face="Times New Roman">{{$a[0]->name}}</font>
      </div>
    </div>
    </div>
  </body>
</html>

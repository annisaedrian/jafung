@extends('layouts.app')

@section('content')
<div class="column">
    <div class="card">
        <div class="card-header">
                    <p class="card-header-title">
                    Kegiaatan Pendidikan dan Pengajaran
                    </p>
        </div>

        <div class="card-content">
          <div class="table__wrapper">
            @role(['superadministrator','admin'])
            <div class="notification is-info">Pastikan sudah mengisi form Unsur Pendidikan dan Unsur Pelaksanaan Pendidikan</div>
            @endrole
            <table class="table is-bordered pricing__table is-fullwidth" id="datatable">
              <thead>
                <tr>
                  <th><abbr title="Position">No</abbr></th>
                  <th>Nama Dosen</th>
                  <th>Jabatan Fungsional</th>
                  <th>Penanggung Jawab</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $n=1 ?>
                <?php foreach ($data as $a): ?>
                  <tr>
                    <td>{{$n}}</td>
                    <td>{{ $a->name }}</td>
                    <td>{{ $a->jab_fungsional }}</td>
                    <td>{{ $a->user->name }}</td>

                    <td>
                      <div class="columns">

                        <div class="column">
                          <a href="pendidikan/{{$a->name}}" class="button is-info">View</a>
                        </div>
                        @role(['superadministrator','admin'])
                        <div class="column">
                          <form action="pendidikan/{{$a->id}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete">
                            <button type="submit" name="button" class="button is-danger">Delete</button>
                          </form>
                        </div>
                        @endrole
                      </div>

                    </td>
                  </tr>
                  <?php $n++ ?>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $('#datatable').DataTable();
  </script>
@endpush

<?php
use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class LaratrustSeeder extends Seeder
{
  public function run()
  {
    $a = new Role();
    $a->name         = 'superadministrator';
    $a->display_name = 'Superadministrator'; // optional
    $a->description  = 'User is allowed to manage and edit other users'; // optional
    $a->save();
    $super = User::create([
        'name' => 'Superadministrator',
        'email' => 'superadministrator@jafung.com',
        'password' => Hash::make('password'),
    ]);
    $super->attachRole($a);

    $b = new Role();
    $b->name         = 'admin';
    $b->display_name = 'Admin'; // optional
    $b->description  = 'User is allowed to manage and edit'; // optional
    $b->save();
    $admin = User::create([
        'name' => 'Admin',
        'email' => 'admin@jafung.com',
        'password' => Hash::make('password'),
    ]);
    $admin->attachRole($b);

    $c = new Role();
    $c->name         = 'dosen';
    $c->display_name = 'Dosen'; // optional
    $c->description  = 'User is allowed to manage and edit'; // optional
    $c->save();
    $dosen = User::create([
        'name' => 'Dosen',
        'email' => 'dosen@jafung.com',
        'password' => Hash::make('password'),
    ]);
    $dosen->attachRole($c);
  }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPenunjangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_penunjangs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('penunjang_id');
            $table->text('kegiatan_penunjang')->nullable();
            $table->string('kedudukan')->nullable();
            $table->string('tempat')->nullable();
            $table->string('tgl')->nullable();
            $table->string('jml_kredit')->nullable();

            $table->foreign('penunjang_id')->references('id')->on('penunjangs')
                ->onUpdate('cascade')->onDelete('cascade');

                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_penunjangs');
    }
}

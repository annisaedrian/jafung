<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnsurPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('unsurpendidikans', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->unsignedInteger('user_id');
          $table->string('nip')->nullable();
          $table->string('nidn')->nullable();
          $table->string('pangkat')->nullable();
          $table->string('gol_ruang')->nullable();
          $table->string('jab_fungsional')->nullable();
          $table->string('tmt')->nullable();
          $table->string('unit_kerja')->nullable();

          $table->foreign('user_id')->references('id')->on('users')
              ->onUpdate('cascade')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unsurpendidikans');
    }
}

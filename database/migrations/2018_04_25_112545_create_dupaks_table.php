<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDupaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dupaks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('unsur');
            $table->string('pengusul_lama');
            $table->string('pengusul_baru');
            $table->string('pengusul_jml');
            $table->string('penilai_lama');
            $table->string('penilai_baru');
            $table->string('penilai_jml');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dupaks');
    }
}

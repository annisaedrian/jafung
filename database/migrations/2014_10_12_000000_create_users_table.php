<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penanggung_jawab')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('nip')->nullable();
            $table->string('nidn')->nullable();
            $table->string('no_karpeg')->nullable();
            $table->string('ttl')->nullable();
            $table->enum('jk', ['Laki - Laki', 'Perempuan']);
            $table->string('pendidikan')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('gol_ruang')->nullable();
            $table->string('tmt')->nullable();
            $table->enum('jab_fungsional', ['Asisten Ahli', 'Lektor','Lektor Kepala','Profesor']);
            $table->string('fakultas')->nullable();
            $table->string('jurusan')->nullable();
            $table->string('masa_kerja')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPenelitiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_penelitians', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('penelitian_id');
            $table->text('judul_karya')->nullable();
            $table->string('nilai_kredit')->nullable();
            $table->string('penilai_perguruan')->nullable();
            $table->string('penilai_pusat')->nullable();

            $table->foreign('penelitian_id')->references('id')->on('penelitians')
                ->onUpdate('cascade')->onDelete('cascade');

                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_penelitians');
    }
}

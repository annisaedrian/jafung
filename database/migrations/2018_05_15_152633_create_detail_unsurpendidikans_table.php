<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailUnsurPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('detail_unsurpendidikans', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('unsurpendidikan_id');
          $table->text('kegiatan_pend')->nullable();
          $table->string('tempat')->nullable();
          $table->string('tgl')->nullable();
          $table->string('jml_kredit')->nullable();

          $table->foreign('unsurpendidikan_id')->references('id')->on('unsurpendidikans')
              ->onUpdate('cascade')->onDelete('cascade');

              $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_unsurpendidikans');
    }
}

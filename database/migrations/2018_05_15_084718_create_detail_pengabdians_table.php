<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPengabdiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('detail_pengabdians', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('pengabdian_id');
          $table->text('kegiatan_pengabdian')->nullable();
          $table->string('bentuk')->nullable();
          $table->string('tempat')->nullable();
          $table->string('tgl')->nullable();
          $table->string('jml_kredit')->nullable();

          $table->foreign('pengabdian_id')->references('id')->on('pengabdians')
              ->onUpdate('cascade')->onDelete('cascade');

              $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pengabdians');
    }
}

<?php

namespace App;

use App\DetailPenunjang;
use Illuminate\Database\Eloquent\Model;

class Penunjang extends Model
{
    protected $table = 'penunjangs';

    protected $fillable = [
        'user_id',
        'name',
        'nip',
        'nidn',
        'pangkat',
        'gol_ruang',
        'jab_fungsional',
        'tmt',
        'unit_kerja'];

    public function detail_penunjang(){
        return $this->hasMany(DetailPenunjang::class, 'penunjang_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

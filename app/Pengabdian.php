<?php

namespace App;

use App\DetailPengabdian;
use Illuminate\Database\Eloquent\Model;

class Pengabdian extends Model
{
    protected $table = 'pengabdians';

    protected $fillable = [
        'user_id',
        'name',
        'nip',
        'nidn',
        'pangkat',
        'gol_ruang',
        'jab_fungsional',
        'tmt',
        'unit_kerja'];

    public function detail_pengabdian(){
        return $this->hasMany(DetailPengabdian::class, 'pengabdian_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

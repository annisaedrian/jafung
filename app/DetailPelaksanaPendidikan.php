<?php

namespace App;

use App\PelaksanaPendidikan;
use Illuminate\Database\Eloquent\Model;

class DetailPelaksanaPendidikan extends Model
{
    protected $table = 'detail_pelaksanapendidikans';

    protected $fillable = [
        'pelaksanapendidikan_id',
        'kegiatan_pend',
        'tempat',
        'tgl',
        'jml_kredit'];

        public function pelaksanapendidikan()
        {
            return $this->belongsTo(PelaksanaPendidikan::class);
        }
}

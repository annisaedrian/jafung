<?php

namespace App;

use App\DetailPenelitian;
use Illuminate\Database\Eloquent\Model;

class Penelitian extends Model
{
    protected $table = 'penelitians';

    protected $fillable = [
        'user_id',
        'name',
        'nip',
        'nidn',
        'pangkat',
        'gol_ruang',
        'jab_fungsional',
        'tmt',
        'unit_kerja'];

    public function detail_penelitian(){
        return $this->hasMany(DetailPenelitian::class, 'penelitian_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function penelitian(){
        return $this->hasMany(Penelitian::class, 'user_id');
    }

    public function pengabdian(){
      return $this->hasMany(Pengabdian::class, 'user_id');
    }

    public function penunjang(){
      return $this->hasMany(Penunjang::class, 'user_id');
    }

    public function unsurpendidikan(){
      return $this->hasMany(UnsurPendidikan::class, 'user_id');
    }

}

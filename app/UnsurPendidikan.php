<?php

namespace App;

use App\DetailUnsurPendidikan;
use Illuminate\Database\Eloquent\Model;

class UnsurPendidikan extends Model
{
    protected $table = 'unsurpendidikans';

    protected $fillable = [
        'user_id',
        'name',
        'nip',
        'nidn',
        'pangkat',
        'gol_ruang',
        'jab_fungsional',
        'tmt',
        'unit_kerja'];

    public function detail_unsurpendidikan(){
        return $this->hasMany(DetailUnsurPendidikan::class, 'unsurpendidikan_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class Select2Controller extends Controller
{
    public function dosen(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("users")
                ->select("id","name")
                ->where('name','LIKE',"%$search%")
                ->get();
        } else {
            $data = DB::table("users")
                ->select("id","name")
                ->get();
        }


        return response()->json($data);
    }
}

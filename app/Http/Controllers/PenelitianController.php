<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use Carbon\Carbon;
use App\Penelitian;
use App\DetailPenelitian;
use Illuminate\Http\Request;

class PenelitianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Penelitian::with('user')->get();
        return view('penelitian.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penelitian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $penelitian = Penelitian::create([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nidn' => $request->nidn,
            'pangkat' => $request->pangkat,
            'gol_ruang' => $request->gol_ruang,
            'jab_fungsional' => $request->jab_fungsional,
            'tmt' => $request->tmt,
            'unit_kerja' => $request->unit_kerja
          ]);
          $total_price = 0;
          $penelitian_arr = [];
          for ($i=0; $i < count($request->judul_karya); $i++) {
            $penelitian_arr[] = [
              'penelitian_id' => $penelitian->id,
              'judul_karya' => $request->judul_karya[$i],
              'nilai_kredit' => $request->nilai_kredit[$i],
              'penilai_perguruan' => $request->penilai_perguruan[$i],
              'penilai_pusat' => $request->penilai_pusat[$i],
            ];
          }
        //   return $penelitian_arr;
          if (DetailPenelitian::insert($penelitian_arr)) {
            return redirect(route('penelitian.index'));
          } else {
            return 'n';
          }
        //   return redirect(route('penelitian.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $data = Penelitian::with('detail_penelitian')->find($id);
            $a = User::where('id' , $data->user_id)->get();
            return view('penelitian.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DetailPenelitian::find($id);
          return view('penelitian.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penelitian = Penelitian::find($id);
        $penelitian->name = $request->input('name');
        $penelitian->user_id = $request->input('user_id');
        $penelitian->nip = $request->input('nip');
        $penelitian->nidn = $request->input('nidn');
        $penelitian->pangkat = $request->input('pangkat');
        $penelitian->gol_ruang = $request->input('gol_ruang');
        $penelitian->jab_fungsional = $request->input('jab_fungsional');
        $penelitian->tmt = $request->input('tmt');
        $penelitian->unit_kerja = $request->input('unit_kerja');
        $penelitian->judul_karya = $request->input('judul_karya');
        $penelitian->nilai_kredit = $request->input('nilai_kredit');
        $penelitian->penilai_perguruan = $request->input('penilai_perguruan');
        $penelitian->penilai_pusat = $request->input('penilai_pusat');
        $penelitian->save();

        return redirect('/penelitian')->with('success', 'Penelitian Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $data = User::where('name', $request->input('name'))->get();
        for ($i=0; $i < count($data); $i++) {
          $a = User::where('id' , $data[$i]->penanggung_jawab)->get();
        }
        return view('penelitian.create',compact('data', 'a'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Penelitian::find($id);
        $id->delete();
        return redirect('/penelitian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_detail($id)
    {
      $data = DetailPenelitian::find($id);
      $data->delete();
      return redirect('/penelitian');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_detail(Request $request, $id)
    {
        DetailPenelitian::where('id', $id)
          ->update([
            'judul_karya' => $request->input('judul_karya'),
            'nilai_kredit' => $request->input('nilai_kredit'),
            'penilai_perguruan' => $request->input('penilai_perguruan'),
            'penilai_pusat' => $request->input('penilai_pusat'),
          ]);

        return redirect('/penelitian');
    }

    public function downloadPDF($id){
        $data = Penelitian::with('detail_penelitian')->find($id);
        $date = Carbon::now();
        $a = User::where('id' , $data->user_id)->get();
        if ($data->jab_fungsional == 'Asisten Ahli') {
          $kredit = (25/100)*$data->detail_penelitian->sum('nilai_kredit');
        } elseif ($data->jab_fungsional == 'Lektor') {
          $kredit = (35/100)*$data->detail_penelitian->sum('nilai_kredit');
        } elseif ($data->jab_fungsional == 'Lektor Kepala') {
          $kredit = (40/100)*$data->detail_penelitian->sum('nilai_kredit');
        }else ($data->jab_fungsional == 'Profesor') {
          $kredit = (45/100)*$data->detail_penelitian->sum('nilai_kredit')
        };

        if ($data->jab_fungsional == 'Asisten Ahli') {
          $kredit_perguruan = (25/100)*$data->detail_penelitian->sum('penilai_perguruan');
        } elseif ($data->jab_fungsional == 'Lektor') {
          $kredit_perguruan = (35/100)*$data->detail_penelitian->sum('penilai_perguruan');
        } elseif ($data->jab_fungsional == 'Lektor Kepala') {
          $kredit_perguruan = (40/100)*$data->detail_penelitian->sum('penilai_perguruan');
        }else ($data->jab_fungsional == 'Profesor') {
          $kredit_perguruan = (45/100)*$data->detail_penelitian->sum('penilai_perguruan')
        };

        if ($data->jab_fungsional == 'Asisten Ahli') {
          $kredit_pusat = (25/100)*$data->detail_penelitian->sum('penilai_pusat');
        } elseif ($data->jab_fungsional == 'Lektor') {
          $kredit_pusat = (35/100)*$data->detail_penelitian->sum('penilai_pusat');
        } elseif ($data->jab_fungsional == 'Lektor Kepala') {
          $kredit_pusat = (40/100)*$data->detail_penelitian->sum('penilai_pusat');
        }else ($data->jab_fungsional == 'Profesor') {
          $kredit_pusat = (45/100)*$data->detail_penelitian->sum('penilai_pusat')
        };
        $pdf = PDF::loadView('penelitian.pdf', compact('data', 'date','a', 'kredit','kredit_perguruan', 'kredit_pusat'));
        return $pdf->stream('penelitian-'.$data->name.'.pdf');

      }
}

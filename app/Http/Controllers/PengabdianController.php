<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use Carbon\Carbon;
use App\Pengabdian;
use App\DetailPengabdian;
use Illuminate\Http\Request;

class PengabdianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = Pengabdian::with('user')->get();
      return view('pengabdian.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengabdian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $pengabdian = Pengabdian::create([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nidn' => $request->nidn,
            'pangkat' => $request->pangkat,
            'gol_ruang' => $request->gol_ruang,
            'jab_fungsional' => $request->jab_fungsional,
            'tmt' => $request->tmt,
            'unit_kerja' => $request->unit_kerja
          ]);
          $total_price = 0;
          $pengabdian_arr = [];
          for ($i=0; $i < count($request->kegiatan_pengabdian); $i++) {
            $pengabdian_arr[] = [
              'pengabdian_id' => $pengabdian->id,
              'kegiatan_pengabdian' => $request->kegiatan_pengabdian[$i],
              'bentuk' => $request->bentuk[$i],
              'tempat' => $request->tempat[$i],
              'tgl' => $request->tgl[$i],
              'jml_kredit' => $request->jml_kredit[$i],
            ];
          }
        //   return $pengabdian_arr;
          if (DetailPengabdian::insert($pengabdian_arr)) {
            return redirect(route('pengabdian.index'));
          } else {
            return 'n';
          }
        //   return redirect(route('pengabdian.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Pengabdian::with('detail_pengabdian')->find($id);
      $a = User::where('id' , $data->user_id)->get();
      return view('pengabdian.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DetailPengabdian::find($id);
          return view('pengabdian.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pengabdian = Pengabdian::find($id);
        $pengabdian->name = $request->input('name');
        $pengabdian->user_id = $request->input('user_id');
        $pengabdian->nip = $request->input('nip');
        $pengabdian->nidn = $request->input('nidn');
        $pengabdian->pangkat = $request->input('pangkat');
        $pengabdian->gol_ruang = $request->input('gol_ruang');
        $pengabdian->jab_fungsional = $request->input('jab_fungsional');
        $pengabdian->tmt = $request->input('tmt');
        $pengabdian->unit_kerja = $request->input('unit_kerja');
        $pengabdian->kegiatan_pengabdian = $request->input('kegiatan_pengabdian');
        $pengabdian->bentuk = $request->input('bentuk');
        $pengabdian->tempat = $request->input('tempat');
        $pengabdian->tgl = $request->input('tgl');
        $pengabdian->jml_kredit = $request->input('jml_kredit');
        $pengabdian->save();

        return redirect('/pengabdian')->with('success', 'Pengabdian Updated');
    }

    public function find(Request $request)
    {
        $data = User::where('name', $request->input('name'))->get();
        for ($i=0; $i < count($data); $i++) {
        $a = User::where('id' , $data[$i]->penanggung_jawab)->get();
        }
        return view('pengabdian.create',compact('data', 'a'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = Pengabdian::find($id);
      $id->delete();
      return redirect('/pengabdian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_detail($id)
    {
      $data = DetailPengabdian::find($id);
      $data->delete();
      return redirect('/pengabdian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_detail(Request $request, $id)
    {
        DetailPengabdian::where('id', $id)
          ->update([
            'kegiatan_pengabdian' => $request->input('kegiatan_pengabdian'),
            'bentuk' => $request->input('bentuk'),
            'tempat' => $request->input('tempat'),
            'tgl' => $request->input('tgl'),
            'jml_kredit' => $request->input('jml_kredit'),
          ]);

        return redirect('/pengabdian');
    }

    public function downloadPDF($id){
        $data = Pengabdian::with('detail_pengabdian')->find($id);
        $date = Carbon::now();
        $a = User::where('id' , $data->user_id)->get();
        if ($data->jab_fungsional == 'Asisten Ahli') {
          $kredit = (10/100)*$data->detail_pengabdian->sum('jml_kredit');
        } elseif ($data->jab_fungsional == 'Lektor') {
          $kredit = (10/100)*$data->detail_pengabdian->sum('jml_kredit');
        } elseif ($data->jab_fungsional == 'Lektor Kepala') {
          $kredit = (10/100)*$data->detail_pengabdian->sum('jml_kredit');
        }else ($data->jab_fungsional == 'Profesor') {
          $kredit = (10/100)*$data->detail_pengabdian->sum('jml_kredit')
        };
        $pdf = PDF::loadView('pengabdian.pdf', compact('data', 'date', 'a', 'kredit'));
        return $pdf->stream('pengabdian-'.$data->name.'.pdf');

      }

}

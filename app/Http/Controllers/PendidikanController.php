<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use Carbon\Carbon;
use App\Pendidikan;
use App\UnsurPendidikan;
use App\DetailUnsurPendidikan;
use App\PelaksanaPendidikan;
use App\DetailPelaksanaPendidikan;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = UnsurPendidikan::all();

      return view('pendidikan.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $pelaksana = PelaksanaPendidikan::with('detail_pelaksanapendidikan')->where('name',$id)->get();
      $unsur = UnsurPendidikan::with('detail_unsurpendidikan')->where('name',$id)->get();
      for ($i=0; $i < count($unsur); $i++) {
        $a = User::where('id' , $unsur[$i]->user_id)->get();
      }
      return view('pendidikan.view',compact('pelaksana', 'unsur', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = UnsurPendidikan::find($id);
      $id = PelaksanaPendidikan::find($id);
      $id->delete();
      return redirect('/pendidikan');
    }

    public function downloadPDF($id){
        $unsur = UnsurPendidikan::with('detail_unsurpendidikan')->where('name',$id)->get();
        if ($unsur[0]->jab_fungsional == 'Asisten Ahli') {
          $kredit_unsur = (55/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
        } elseif ($unsur[0]->jab_fungsional == 'Lektor') {
          $kredit_unsur = (45/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
        } elseif ($unsur[0]->jab_fungsional == 'Lektor Kepala') {
          $kredit_unsur = (40/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
        }else ($unsur[0]->jab_fungsional == 'Profesor') {
          $kredit_unsur = (35/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit')
        };

        $pelaksana = PelaksanaPendidikan::with('detail_pelaksanapendidikan')->where('name',$id)->get();
        if ($pelaksana[0]->jab_fungsional == 'Asisten Ahli') {
          $kredit_pelaksana = (55/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
        } elseif ($pelaksana[0]->jab_fungsional == 'Lektor') {
          $kredit_pelaksana = (45/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
        } elseif ($pelaksana[0]->jab_fungsional == 'Lektor Kepala') {
          $kredit_pelaksana = (40/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
        }else ($pelaksana[0]->jab_fungsional == 'Profesor') {
          $kredit_pelaksana = (35/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit')
        };

        $a = User::where('id' , $unsur[0]->user_id)->get();
        $date = Carbon::now();
        $pdf = PDF::loadView('pendidikan.pdf', compact('unsur', 'pelaksana', 'date','a', 'kredit_pelaksana', 'kredit_unsur'));
        return $pdf->stream('pendidikan-'.$unsur[0]->name.'.pdf');

      }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\UnsurPendidikan;
use App\DetailUnsurPendidikan;
use Illuminate\Http\Request;

class UnsurPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = UnsurPendidikan::with('user')->get();
      return view('unsurpendidikan.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unsurpendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unsurpendidikan = UnsurPendidikan::create([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nidn' => $request->nidn,
            'pangkat' => $request->pangkat,
            'gol_ruang' => $request->gol_ruang,
            'jab_fungsional' => $request->jab_fungsional,
            'tmt' => $request->tmt,
            'unit_kerja' => $request->unit_kerja
          ]);
          $total_price = 0;
          $unsurpendidikan_arr = [];
          for ($i=0; $i < count($request->kegiatan_pend); $i++) {
            $unsurpendidikan_arr[] = [
              'unsurpendidikan_id' => $unsurpendidikan->id,
              'kegiatan_pend' => $request->kegiatan_pend[$i],
              'tempat' => $request->tempat[$i],
              'tgl' => $request->tgl[$i],
              'jml_kredit' => $request->jml_kredit[$i],
            ];
          }
        //   return $unsurpendidikan_arr;
          if (DetailUnsurPendidikan::insert($unsurpendidikan_arr)) {
            return redirect(route('unsurpendidikan.index'));
          } else {
            return 'n';
          }
        //   return redirect(route('unsurpendidikan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = UnsurPendidikan::with('detail_unsurpendidikan')->find($id);
      $a = User::where('id' , $data->user_id)->get();
      return view('unsurpendidikan.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DetailUnsurPendidikan::find($id);
          return view('unsurpendidikan.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unsurpendidikan = UnsurPendidikan::find($id);
        $unsurpendidikan->name = $request->input('name');
        $unsurpendidikan->user_id = $request->input('user_id');
        $unsurpendidikan->nip = $request->input('nip');
        $unsurpendidikan->nidn = $request->input('nidn');
        $unsurpendidikan->pangkat = $request->input('pangkat');
        $unsurpendidikan->gol_ruang = $request->input('gol_ruang');
        $unsurpendidikan->jab_fungsional = $request->input('jab_fungsional');
        $unsurpendidikan->tmt = $request->input('tmt');
        $unsurpendidikan->kegiatan_pend = $request->input('kegiatan_pend');
        $unsurpendidikan->tempat = $request->input('tempat');
        $unsurpendidikan->tgl = $request->input('tgl');
        $unsurpendidikan->jml_kredit = $request->input('jml_kredit');
        $unsurpendidikan->save();

        return redirect('/unsurpendidikan')->with('success', 'UnsurPendidikan Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $data = User::where('name', $request->input('name'))->get();
        for ($i=0; $i < count($data); $i++) {
          $a = User::where('id' , $data[$i]->penanggung_jawab)->get();
        }
        return view('unsurpendidikan.create',compact('data', 'a'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = UnsurPendidikan::find($id);
      $id->delete();
      return redirect('/unsurpendidikan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_detail($id)
    {
      $data = DetailUnsurPendidikan::find($id);
      $data->delete();
      return redirect('/unsurpendidikan');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update_detail(Request $request, $id)
   {
       DetailUnsurPendidikan::where('id', $id)
         ->update([
           'kegiatan_pend' => $request->input('kegiatan_pend'),
           'tempat' => $request->input('tempat'),
           'tgl' => $request->input('tgl'),
           'jml_kredit' => $request->input('jml_kredit'),
         ]);

       return redirect('/unsurpendidikan');
   }

}

<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use Carbon\Carbon;
use App\Penunjang;
use App\DetailPenunjang;
use Illuminate\Http\Request;

class PenunjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = Penunjang::with('user')->get();
      return view('penunjang.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penunjang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $penunjang = Penunjang::create([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nidn' => $request->nidn,
            'pangkat' => $request->pangkat,
            'gol_ruang' => $request->gol_ruang,
            'jab_fungsional' => $request->jab_fungsional,
            'tmt' => $request->tmt,
            'unit_kerja' => $request->unit_kerja
          ]);
          $total_price = 0;
          $penunjang_arr = [];
          for ($i=0; $i < count($request->kegiatan_penunjang); $i++) {
            $penunjang_arr[] = [
              'penunjang_id' => $penunjang->id,
              'kegiatan_penunjang' => $request->kegiatan_penunjang[$i],
              'kedudukan' => $request->kedudukan[$i],
              'tempat' => $request->tempat[$i],
              'tgl' => $request->tgl[$i],
              'jml_kredit' => $request->jml_kredit[$i],
            ];
          }
        //   return $penunjang_arr;
          if (DetailPenunjang::insert($penunjang_arr)) {
            return redirect(route('penunjang.index'));
          } else {
            return 'n';
          }
        //   return redirect(route('penunjang.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Penunjang::with('detail_penunjang')->find($id);
      $a = User::where('id' , $data->user_id)->get();
      return view('penunjang.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DetailPenunjang::find($id);
          return view('penunjang.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penunjang = Penunjang::find($id);
        $penunjang->name = $request->input('name');
        $penunjang->user_id = $request->input('user_id');
        $penunjang->nip = $request->input('nip');
        $penunjang->nidn = $request->input('nidn');
        $penunjang->pangkat = $request->input('pangkat');
        $penunjang->gol_ruang = $request->input('gol_ruang');
        $penunjang->jab_fungsional = $request->input('jab_fungsional');
        $penunjang->tmt = $request->input('tmt');
        $penunjang->unit_kerja = $request->input('unit_kerja');
        $penunjang->kegiatan_penunjang = $request->input('kegiatan_penunjang');
        $penunjang->kedudukan = $request->input('kedudukan');
        $penunjang->tempat = $request->input('tempat');
        $penunjang->tgl = $request->input('tgl');
        $penunjang->jml_kredit = $request->input('jml_kredit');
        $penunjang->save();

        return redirect('/penunjang')->with('success', 'Penunjang Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $data = User::where('name', $request->input('name'))->get();
        for ($i=0; $i < count($data); $i++) {
          $a = User::where('id' , $data[$i]->penanggung_jawab)->get();
        }
        return view('penunjang.create',compact('data', 'a'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Penunjang::find($id);
      $data->delete();
      return redirect('/penunjang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_detail($id)
    {
      $data = DetailPenunjang::find($id);
      $data->delete();
      return redirect('/penunjang');
    }

    public function update_detail(Request $request, $id)
    {
        DetailPenunjang::where('id', $id)
          ->update([
            'kegiatan_penunjang' => $request->input('kegiatan_penunjang'),
            'kedudukan' => $request->input('kedudukan'),
            'tempat' => $request->input('tempat'),
            'tgl' => $request->input('tgl'),
            'jml_kredit' => $request->input('jml_kredit'),
          ]);

        return redirect('/penunjang');
    }

    public function downloadPDF($id){
        $data = Penunjang::with('detail_penunjang')->find($id);
        $date = Carbon::now();
        if ($data->jab_fungsional == 'Asisten Ahli') {
          $kredit = (10/100)*$data->detail_penunjang->sum('jml_kredit');
        } elseif ($data->jab_fungsional == 'Lektor') {
          $kredit = (10/100)*$data->detail_penunjang->sum('jml_kredit');
        } elseif ($data->jab_fungsional == 'Lektor Kepala') {
          $kredit = (10/100)*$data->detail_penunjang->sum('jml_kredit');
        }else ($data->jab_fungsional == 'Profesor') {
          $kredit = (10/100)*$data->detail_penunjang->sum('jml_kredit')
        };
        $a = User::where('id' , $data->user_id)->get();
        $pdf = PDF::loadView('penunjang.pdf', compact('data', 'date','a', 'kredit'));
        return $pdf->stream('penunjang-'.$data->name.'.pdf');

      }
}

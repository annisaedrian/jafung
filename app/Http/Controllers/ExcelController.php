<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use App\User;

class ExcelController extends Controller
{
    public function export()

	{

		return view('export');

	}

	public function dosen($type)

	{
		$data = User::get()->toArray();
		return Excel::create('Data Profile Dosen', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);

	}

}

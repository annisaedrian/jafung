<?php

namespace App\Http\Controllers;

use App\User;
use App\PelaksanaPendidikan;
use App\DetailPelaksanaPendidikan;
use Illuminate\Http\Request;

class PelaksanaPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = PelaksanaPendidikan::with('user')->get();
      return view('pelaksanapendidikan.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('pelaksanapendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pelaksanapendidikan = PelaksanaPendidikan::create([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nidn' => $request->nidn,
            'pangkat' => $request->pangkat,
            'gol_ruang' => $request->gol_ruang,
            'jab_fungsional' => $request->jab_fungsional,
            'tmt' => $request->tmt,
            'unit_kerja' => $request->unit_kerja
          ]);
          $total_price = 0;
          $pelaksanapendidikan_arr = [];
          for ($i=0; $i < count($request->kegiatan_pend); $i++) {
            $pelaksanapendidikan_arr[] = [
              'pelaksanapendidikan_id' => $pelaksanapendidikan->id,
              'kegiatan_pend' => $request->kegiatan_pend[$i],
              'tempat' => $request->tempat[$i],
              'tgl' => $request->tgl[$i],
              'jml_kredit' => $request->jml_kredit[$i],
            ];
          }
        //   return $pelaksanapendidikan_arr;
          if (DetailPelaksanaPendidikan::insert($pelaksanapendidikan_arr)) {
            return redirect(route('pelaksanapendidikan.index'));
          } else {
            return 'n';
          }
        //   return redirect(route('pelaksanapendidikan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = PelaksanaPendidikan::with('detail_pelaksanapendidikan')->find($id);
      $a = User::where('id' , $data->user_id)->get();
      return view('pelaksanapendidikan.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = DetailPelaksanaPendidikan::find($id);
          return view('pelaksanapendidikan.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pelaksanapendidikan = PelaksanaPendidikan::find($id);
      $pelaksanapendidikan->name = $request->input('name');
      $pelaksanapendidikan->user_id = $request->input('user_id');
      $pelaksanapendidikan->nip = $request->input('nip');
      $pelaksanapendidikan->nidn = $request->input('nidn');
      $pelaksanapendidikan->pangkat = $request->input('pangkat');
      $pelaksanapendidikan->gol_ruang = $request->input('gol_ruang');
      $pelaksanapendidikan->jab_fungsional = $request->input('jab_fungsional');
      $pelaksanapendidikan->tmt = $request->input('tmt');
      $pelaksanapendidikan->kegiatan_pend = $request->input('kegiatan_pend');
      $pelaksanapendidikan->tempat = $request->input('tempat');
      $pelaksanapendidikan->tgl = $request->input('tgl');
      $pelaksanapendidikan->jml_kredit = $request->input('jml_kredit');
      $pelaksanapendidikan->save();

      return redirect('/pelaksanapendidikan')->with('success', 'PelaksanaPendidikan Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $data = User::where('name', $request->input('name'))->get();
        for ($i=0; $i < count($data); $i++) {
          $a = User::where('id' , $data[$i]->penanggung_jawab)->get();
        }
        return view('pelaksanapendidikan.create',compact('data', 'a'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = PelaksanaPendidikan::find($id);
      $id->delete();
      return redirect('/pelaksanapendidikan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_detail($id)
    {
      $data = DetailPelaksanaPendidikan::find($id);
      $data->delete();
      return redirect('/pelaksanapendidikan');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update_detail(Request $request, $id)
   {
       DetailPelaksanaPendidikan::where('id', $id)
         ->update([
           'kegiatan_pend' => $request->input('kegiatan_pend'),
           'tempat' => $request->input('tempat'),
           'tgl' => $request->input('tgl'),
           'jml_kredit' => $request->input('jml_kredit'),
         ]);

       return redirect('/pelaksanapendidikan');
   }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all();
        return view('dosen.index')->with('data', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $dosen = new User;
        $dosen->name = $request->input('name');
        $dosen->penanggung_jawab = $request->input('penanggung_jawab');
        $dosen->email = $request->input('email');
        $dosen->password = Hash::make('password');
        $dosen->nip = $request->input('nip');
        $dosen->nidn = $request->input('nidn');
        $dosen->no_karpeg = $request->input('no_karpeg');
        $dosen->ttl = $request->input('ttl');
        $dosen->jk = $request->input('jk');
        $dosen->pendidikan = $request->input('pendidikan');
        $dosen->pangkat = $request->input('pangkat');
        $dosen->gol_ruang = $request->input('gol_ruang');
        $dosen->jab_fungsional = $request->input('jab_fungsional');
        $dosen->tmt = $request->input('tmt');
        $dosen->fakultas = $request->input('fakultas');
        $dosen->jurusan = $request->input('jurusan');
        $dosen->masa_kerja = $request->input('masa_kerja');
        $dosen->unit_kerja = $request->input('unit_kerja');
        $dosen->save();
        $dosen->attachRole('dosen');

        return redirect(route('dosen.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::find($id);
         $a = User::where('id' , $data->penanggung_jawab)->get();
            return view('dosen.view',compact('data', 'a'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
            return view('dosen.edit')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dosen = User::find($id);
        $dosen->name = $request->input('name');
        $dosen->penanggung_jawab = $request->input('penanggung_jawab');
        $dosen->email = $request->input('email');
        $dosen->password = Hash::make($request->input('password'));
        $dosen->nip = $request->input('nip');
        $dosen->nidn = $request->input('nidn');
        $dosen->no_karpeg = $request->input('no_karpeg');
        $dosen->ttl = $request->input('ttl');
        $dosen->jk = $request->input('jk');
        $dosen->pendidikan = $request->input('pendidikan');
        $dosen->pangkat = $request->input('pangkat');
        $dosen->gol_ruang = $request->input('gol_ruang');
        $dosen->jab_fungsional = $request->input('jab_fungsional');
        $dosen->tmt = $request->input('tmt');
        $dosen->fakultas = $request->input('fakultas');
        $dosen->jurusan = $request->input('jurusan');
        $dosen->masa_kerja = $request->input('masa_kerja');
        $dosen->unit_kerja = $request->input('unit_kerja');
        $dosen->save();

        return redirect('/dosen')->with('success', 'Dosen Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = User::find($id);
        $id->delete();
        return redirect('/dosen');
    }
}

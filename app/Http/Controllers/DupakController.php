<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use App\Dupak;
use App\UnsurPendidikan;
use App\PelaksanaPendidikan;
use App\Penelitian;
use App\Pengabdian;
use App\Penunjang;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DupakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = User::all();
      return view('dupak.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dupak.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $pelaksana = PelaksanaPendidikan::with('detail_pelaksanapendidikan')->where('name',$id)->get();
      $unsur = UnsurPendidikan::with('detail_unsurpendidikan')->where('name',$id)->get();
      $penelitian = Penelitian::with('detail_penelitian')->where('name',$id)->get();
      $pengabdian = Pengabdian::with('detail_pengabdian')->where('name',$id)->get();
      $penunjang = Penunjang::with('detail_penunjang')->where('name',$id)->get();
      $user = User::where('name',$id)->get();
      for ($i=0; $i < count($unsur); $i++) {
        $a = User::where('id' , $unsur[$i]->user_id)->get();
      }
      return view('dupak.view',compact(
        'pelaksana',
        'unsur',
        'user',
        'a',
        'penelitian',
        'pengabdian',
        'penunjang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = UnsurPendidikan::find($id);
      $id = PelaksanaPendidikan::find($id);
      $id = Penunjang::find($id);
      $id = Penelitian::find($id);
      $id = Penunjang::find($id);
      $id = User::find($id);
      $id->delete();
      return redirect('/dupak');
    }

    public function downloadPDF($id){
      $user = User::where('name', $id)->get();

      $unsur = UnsurPendidikan::with('detail_unsurpendidikan')->where('name',$id)->get();
      if ($unsur[0]->jab_fungsional == 'Asisten Ahli') {
        $kredit_unsur = (55/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
      } elseif ($unsur[0]->jab_fungsional == 'Lektor') {
        $kredit_unsur = (45/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
      } elseif ($unsur[0]->jab_fungsional == 'Lektor Kepala') {
        $kredit_unsur = (40/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit');
      }else ($unsur[0]->jab_fungsional == 'Profesor') {
        $kredit_unsur = (35/100)*$unsur[0]->detail_unsurpendidikan->sum('jml_kredit')
      };

      $pelaksana = PelaksanaPendidikan::with('detail_pelaksanapendidikan')->where('name',$id)->get();
      if ($pelaksana[0]->jab_fungsional == 'Asisten Ahli') {
        $kredit_pelaksana = (55/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
      } elseif ($pelaksana[0]->jab_fungsional == 'Lektor') {
        $kredit_pelaksana = (45/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
      } elseif ($pelaksana[0]->jab_fungsional == 'Lektor Kepala') {
        $kredit_pelaksana = (40/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit');
      }else ($pelaksana[0]->jab_fungsional == 'Profesor') {
        $kredit_pelaksana = (35/100)*$pelaksana[0]->detail_pelaksanapendidikan->sum('jml_kredit')
      };

      // $penelitian = Penelitian::with('detail_penelitian')->where('name',$id)->get();
      // if ($penelitian[0]->jab_fungsional == 'Asisten Ahli') {
      //   $kredit_penelitian = (25/100)*$penelitian[0]->detail_penelitian->sum('nilai_kredit');
      // } elseif ($penelitian[0]->jab_fungsional == 'Lektor') {
      //   $kredit_penelitian = (35/100)*$penelitian[0]->detail_penelitian->sum('nilai_kredit');
      // } elseif ($penelitian[0]->jab_fungsional == 'Lektor Kepala') {
      //   $kredit_penelitian = (40/100)*$penelitian[0]->detail_penelitian->sum('nilai_kredit');
      // }else ($penelitian[0]->jab_fungsional == 'Profesor') {
      //   $kredit_penelitian = (45/100)*$penelitian[0]->detail_penelitian->sum('nilai_kredit')
      // };
      //
      // $pengabdian = Pengabdian::with('detail_pengabdian')->where('name',$id)->get();
      // if ($pengabdian->jab_fungsional == 'Asisten Ahli') {
      //   $kredit_pengabdian = (10/100)*$pengabdian->detail_pengabdian->sum('jml_kredit');
      // } elseif ($pengabdian->jab_fungsional == 'Lektor') {
      //   $kredit_pengabdian = (10/100)*$pengabdian->detail_pengabdian->sum('jml_kredit');
      // } elseif ($pengabdian->jab_fungsional == 'Lektor Kepala') {
      //   $kredit_pengabdian = (10/100)*$pengabdian->detail_pengabdian->sum('jml_kredit');
      // }else ($pengabdian->jab_fungsional == 'Profesor') {
      //   $kredit_pengabdian = (10/100)*$pengabdian->detail_pengabdian->sum('jml_kredit')
      // };
      //
      // $penunjang = Penunjang::with('detail_penunjang')->where('name',$id)->get();
      // if ($penunjang->jab_fungsional == 'Asisten Ahli') {
      //   $kredit_penunjang = (10/100)*$penunjang->detail_penunjang->sum('jml_kredit');
      // } elseif ($penunjang->jab_fungsional == 'Lektor') {
      //   $kredit_penunjang = (10/100)*$penunjang->detail_penunjang->sum('jml_kredit');
      // } elseif ($penunjang->jab_fungsional == 'Lektor Kepala') {
      //   $kredit_penunjang = (10/100)*$penunjang->detail_penunjang->sum('jml_kredit');
      // }else ($penunjang->jab_fungsional == 'Profesor') {
      //   $kredit_penunjang = (10/100)*$penunjang->detail_penunjang->sum('jml_kredit')
      // };

        $a = User::where('id' , $unsur[0]->user_id)->get();
        $date = Carbon::now();
        $pdf = PDF::loadView('dupak.pdf', compact(
          'unsur',
          'pelaksana',
          'penelitian',
          'pengabdian',
          'penunjang',
          'date',
          'user',
          'a',
          'kredit_penelitian',
          'kredit_penunjang',
          'kredit_pengabdian',
          'kredit_pelaksana',
          'kredit_unsur'));
        return $pdf->stream('dupak-'.$unsur[0]->name.'.pdf');

      }
}

<?php

namespace App;

use App\DetailPelaksanaPendidikan;
use Illuminate\Database\Eloquent\Model;

class PelaksanaPendidikan extends Model
{
    protected $table = 'pelaksanapendidikans';

    protected $fillable = [
        'user_id',
        'name',
        'nip',
        'nidn',
        'pangkat',
        'gol_ruang',
        'jab_fungsional',
        'tmt',
        'unit_kerja'];

    public function detail_pelaksanapendidikan(){
        return $this->hasMany(DetailPelaksanaPendidikan::class, 'pelaksanapendidikan_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

<?php

namespace App;

use App\Penunjang;
use Illuminate\Database\Eloquent\Model;

class DetailPenunjang extends Model
{
    protected $table = 'detail_penunjangs';

    protected $fillable = [
        'penunjang_id',
        'kegiatan_penunjang',
        'kedudukan',
        'tempat',
        'tgl',
        'jml_kredit'
      ];

        public function penunjang()
        {
            return $this->belongsTo(Penunjang::class);
        }
}

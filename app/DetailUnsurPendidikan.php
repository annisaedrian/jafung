<?php

namespace App;

use App\UnsurPendidikan;
use Illuminate\Database\Eloquent\Model;

class DetailUnsurPendidikan extends Model
{
    protected $table = 'detail_unsurpendidikans';

    protected $fillable = [
        'unsurpendidikan_id',
        'kegiatan_pend',
        'tempat',
        'tgl',
        'jml_kredit'];

        public function unsurpendidikan()
        {
            return $this->belongsTo(UnsurPendidikan::class);
        }
}

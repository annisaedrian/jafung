<?php

namespace App;

use App\Penelitian;
use Illuminate\Database\Eloquent\Model;

class DetailPenelitian extends Model
{
    protected $table = 'detail_penelitians';

    protected $fillable = [
        'penelitian_id',
        'judul_karya', 
        'nilai_kredit', 
        'penilaian_perguruan', 
        'penilaian_pusat'];

        public function penelitian()
        {
            return $this->belongsTo(Penelitian::class);
        }
}

<?php

namespace App;

use App\Pengabdian;
use Illuminate\Database\Eloquent\Model;

class DetailPengabdian extends Model
{
    protected $table = 'detail_pengabdians';

    protected $fillable = [
        'pengabdian_id',
        'kegiatan_pengabdian',
        'bentuk',
        'tempat',
        'tgl',
        'jml_kredit',
        'ket_bukti'
      ];

        public function pengabdian()
        {
            return $this->belongsTo(Pengabdian::class);
        }
}
